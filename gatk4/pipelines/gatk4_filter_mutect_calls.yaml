info:
  description: GATK filter MuTect2 pipeline 
  date:        14/11/2019
  api: 2.0.0
  arguments:
    orientation_model:
      File name for the orientation bias model table
    contamination_table:
      File name for the cross-sample contamination table 
    vcf_filtered:
      Output filtered VCF file
    pileup_summary_tumor:
      File name for the tumor pileup summary table
    pileup_summary_normal:
      File name for the tumor pileup summary table
    bam_normal:
      Normal BAM file
    bam_tumor:
      Tumor BAM file
    vcf_output:
      MuTect2 unfiltered VCF output file
    stats_output:
      Merged MuTect2 stats output file
    intervals:
      Scattered regions used to run MuTect2
    tmp_dir:
      Temporary directory
  batches:
    intervals:
      snippet: gatk_mutect2
      required:
        - --intervals
  defaults:
    tmp_dir: /scratch
items:
  - name: gatk_filter_mutect_calls
    type: snippet
    arguments:
      - prefix: -i
        pipeline_arg: '%(vcf_output)s'
      - prefix: -o
        pipeline_arg: '%(vcf_filtered)s'
      - prefix: -m
        pipeline_arg: '%(stats_output)s'
      - prefix: -c
        pipeline_arg: '%(contamination_table)s'
      - prefix: -s
        pipeline_arg:
          snippet_name: gatk_calculate_contamination
          result_key: segments
          result_arguments:
            - prefix : -o
              pipeline_arg: '%(contamination_table)s'
        type: composite_arg
      - prefix: -a
        pipeline_arg: '%(orientation_model)s'
    dependencies:
      items:
        - name: gatk_learn_read_orientation_model
          type: snippet
          arguments:
            - prefix: -i
              pipeline_arg:
                snippet_name: gatk_mutect2
                result_key: f1r2
                result_arguments:
                  - prefix: input_batch
                    pipeline_arg: '%(intervals)s'
                    type: batch_file_arg
                  - prefix: -o
                    pipeline_arg: '%(vcf_output)s'
              type: composite_arg
            - prefix: -o
              pipeline_arg: '%(orientation_model)s'
        - name: gatk_merge_vcfs
          type: snippet
          arguments:
            - prefix: -o
              pipeline_arg: '%(vcf_output)s'
            - prefix: -t
              pipeline_arg: '%(tmp_dir)s'
            - prefix: -i
              pipeline_arg:
                snippet_name: gatk_mutect2
                result_key: vcf
                result_arguments:
                  - prefix: input_batch
                    pipeline_arg: '%(intervals)s'
                    type: batch_file_arg
                  - prefix: -o
                    pipeline_arg: '%(vcf_output)s'
              type: composite_arg
        - name: gatk_merge_mutect_stats
          type: snippet
          arguments:
            - prefix: -o
              pipeline_arg: '%(stats_output)s'
            - prefix: -t
              pipeline_arg: '%(tmp_dir)s'
            - prefix: -i
              pipeline_arg:
                snippet_name: gatk_mutect2
                result_key: stats
                result_arguments:
                  - prefix: input_batch
                    pipeline_arg: '%(intervals)s'
                    type: batch_file_arg
                  - prefix: -o
                    pipeline_arg: '%(vcf_output)s'
              type: composite_arg
        - name: gatk_calculate_contamination
          type: snippet
          arguments:
            - prefix: -n
              pipeline_arg: '%(pileup_summary_normal)s'
            - prefix: -t
              pipeline_arg: '%(pileup_summary_tumor)s'
            - prefix: -o
              pipeline_arg: '%(contamination_table)s'
          dependencies:
            items:
              - name: gatk_gather_pileup_summaries
                type: snippet
                arguments:
                  - prefix: -o
                    pipeline_arg: '%(pileup_summary_normal)s'
                  - prefix: -t
                    pipeline_arg: '%(tmp_dir)s'
                  - prefix: -i
                    pipeline_arg:
                      snippet_name: gatk_pileup_summary
                      result_key: table
                      result_arguments:
                        - prefix: input_batch
                          pipeline_arg: '%(intervals)s'
                          type: batch_file_arg
                        - prefix: -o
                          pipeline_arg: '%(pileup_summary_normal)s'
                    type: composite_arg
                dependencies:
                  items:
                    - name: gatk_pileup_summary
                      type: batch_snippet
                      arguments:
                        - prefix: -o
                          pipeline_arg: '%(pileup_summary_normal)s'
                        - prefix: input_batch
                          pipeline_arg: '%(intervals)s'
                          type: batch_file_arg
                        - prefix: -i
                          pipeline_arg: '%(bam_normal)s'
              - name: gatk_gather_pileup_summaries
                type: snippet
                arguments:
                  - prefix: -o
                    pipeline_arg: '%(pileup_summary_tumor)s'
                  - prefix: -t
                    pipeline_arg: '%(tmp_dir)s'
                  - prefix: -i
                    pipeline_arg:
                      snippet_name: gatk_pileup_summary
                      result_key: table
                      result_arguments:
                        - prefix: input_batch
                          pipeline_arg: '%(intervals)s'
                          type: batch_file_arg
                        - prefix: -o
                          pipeline_arg: '%(pileup_summary_tumor)s'
                    type: composite_arg
                dependencies:
                  items:
                    - name: gatk_pileup_summary
                      type: batch_snippet
                      arguments:
                        - prefix: -o
                          pipeline_arg: '%(pileup_summary_tumor)s'
                        - prefix: -i
                          pipeline_arg: '%(bam_tumor)s'
                        - prefix: input_batch
                          pipeline_arg: '%(intervals)s'
                          type: batch_file_arg
