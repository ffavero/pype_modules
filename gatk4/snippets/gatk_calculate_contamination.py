##
# cSpell:word pype, ncpu, gatk, segs
import os
import re
from pype.misc import bases_format
from pype.process import Command


def requirements():
    return({'ncpu': 1, 'time': '48:00:00', 'mem': '8gb'})


def results(argv):
    try:
        output = argv['--out']
    except KeyError:
        output = argv['-o']
    segments = re.sub('.gz', '', output)
    segments = re.sub('.txt', '', output)
    return({
        'table': output,
        'segments': '%s_segs.txt' % segments
    })


def friendly_name(argv):
    try:
        rec_name = argv['--out']
    except KeyError:
        rec_name = argv['-o']
    rec_basename = os.path.basename(rec_name)
    return('%s_%s' % ('gatk_contest', rec_basename))


def add_parser(subparsers, module_name):
    return subparsers.add_parser(
        module_name, help=(
            'Calculate the fraction of reads coming '
            'from cross-sample contamination'),
        add_help=False)


def gatk_calculate_contamination_args(parser, argv):
    parser.add_argument('-t', '--tumor', dest='tumor',
                        help='input tumor pileup', required=True)
    parser.add_argument('-n', '--normal', dest='normal',
                        help='input normal pileup')
    parser.add_argument('-o', '--out', dest='out',
                        help='output contamination table',
                        required=True)
    return parser.parse_args(argv)


def gatk_calculate_contamination(subparsers, module_name,
                                 argv, profile, log):

    log.log.info('Process snippets arguments')
    args = gatk_calculate_contamination_args(add_parser(
        subparsers, module_name), argv)

    output = args.out
    segments = re.sub('.gz', '', output)
    segments = re.sub('.txt', '', output)
    segments = '%s_segs.txt' % segments

    req = requirements()
    try:
        mem = int(bases_format(req['mem']) / (1000 * 1000))
    except KeyError:
        mem = 4000
    # remove 500Mb memory to run the command
    mem = mem - 500
    log.log.info('Set Java memory limit to %im' % mem)

    log.log.info('Preparing gatk command line')
    gatk_cmd = ('gatk --java-options "-Xmx%(memory)im" '
                'CalculateContamination '
                '-I  %(tumor)s '
                '-segments %(segments)s '
                '-O %(output)s') % {
                    'memory': mem,
                    'tumor': args.tumor,
                    'segments': segments,
                    'output': output
                }

    if args.normal:
        gatk_cmd = '%s -matched %s' % (gatk_cmd, args.normal)

    gatk = Command(gatk_cmd, log, profile, name='calculate_contamination')

    gatk.add_namespace(profile.programs['gatk4'])

    gatk.add_input(args.tumor)
    if args.normal:
        gatk.add_input(args.normal)

    gatk.add_output(output)
    gatk.add_output(segments)

    gatk.run()
    gatk.close()
