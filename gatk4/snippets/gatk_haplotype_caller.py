##
# cSpell:word pype, gatk, ncpu, nargs, haplotype, haplo, tabix, bgzip

import os
import re
from pype.misc import bases_format
from pype.process import Command


def requirements():
    return({'ncpu': 1, 'time': '48:00:00', 'mem': '16gb'})


def results(argv):
    try:
        output = argv['--out']
    except KeyError:
        output = argv['-o']
    if '--intervals' in argv.keys() or '-l' in argv.keys():
        try:
            intervals = argv['--intervals']
        except KeyError:
            intervals = argv['-l']
    else:
        intervals = None
    if intervals == 'None':
        intervals = None
    output = '%s' % re.sub('.gz$', '', output)
    output = '%s' % re.sub('.vcf$', '', output)
    output = '%s' % re.sub('.bcf$', '', output)

    if intervals is not None:
        if os.path.isfile(intervals):
            intervals_name = os.path.basename(intervals)
        else:
            intervals_name = intervals.replace(
                ':', '_').replace('-', '_')
        output = '%s_%s' % (
            output, intervals_name)
        output = os.path.join(
            os.path.dirname(output), 'partials',
            os.path.basename(output))
    output = '%s.vcf.gz' % output
    return({'vcf': output, 'idx': '%s.tbi' % output})


def friendly_name(argv):
    res = results(argv)
    rec_basename = os.path.basename(res['vcf'])
    return('%s_%s' % ('gatk_haplotype_caller', rec_basename))


def add_parser(subparsers, module_name):
    return subparsers.add_parser(
        module_name, help='Call variant with gatk HaplotypeCaller',
        add_help=False)


def gatk_haplo_args(parser, argv):
    parser.add_argument('-i', '--input', dest='input',
                        help='input bam file', required=True)
    parser.add_argument('-m', '--min-mapping-quality', dest='min_map',
                        help=('Exclude alignments from analysis if '
                              'they have a mapping quality less than MIN_MAP'),
                        type=int, default=30)
    parser.add_argument('-q', '--min-base-quality', dest='min_qual',
                        help=('Exclude alleles from analysis if their '
                              'supporting base quality is less than MIN_QUAL'),
                        type=int, default=20)
    parser.add_argument('-o', '--out', dest='out',
                        help='Output vcf file', required=True)
    parser.add_argument('-l', '--intervals', dest='intervals',
                        help='chromosome location(s) where to run the caller')
    return parser.parse_args(argv)


def gatk_haplotype_caller(subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = gatk_haplo_args(add_parser(
        subparsers, module_name), argv)

    build = profile.genome_build
    log.log.info('Use genome build %s' % build)

    genome = profile.files['genome_fa']
    log.log.info('Use genome reference %s' % genome)

    output = '%s' % re.sub(r'\.gz$', '', args.out)
    output = '%s' % re.sub(r'\.vcf$', '', output)
    output = '%s' % re.sub(r'\.bcf$', '', output)

    intervals = args.intervals
    if intervals is not None:
        if os.path.isfile(intervals):
            intervals_name = os.path.basename(intervals)
        else:
            intervals_name = intervals.replace(
                ':', '_').replace('-', '_')
        output = '%s_%s' % (
            output, intervals_name)
        output = os.path.join(
            os.path.dirname(output), 'partials',
            os.path.basename(output))
    output = '%s.vcf.gz' % output

    req = requirements()
    try:
        mem = int(bases_format(req['mem']) / (1000 * 1000))
    except KeyError:
        mem = 4000
    # remove 500Mb memory to run the command
    mem = mem - 500
    log.log.info('Set Java memory limit to %im' % mem)

    log.log.info('Preparing gatk command line')
    gatk_cmd = ('gatk --java-options "-Xmx%(memory)im" '
                'HaplotypeCaller -R %(genome)s '
                '--minimum-mapping-quality %(min_map)s '
                '--min-base-quality-score %(min_qual)s '
                '-I %(input)s -O /dev/stdout') % {
                    'memory': mem,
                    'genome': genome,
                    'min_map': args.min_map,
                    'min_qual': args.min_qual,
                    'input': args.input
                }

    if args.intervals:
        gatk_cmd = '%s -L %s' % (gatk_cmd, args.intervals)

    bgzip_cmd = 'bgzip -c'
    tabix_cmd = 'tabix -p vcf -f %s' % output

    gatk = Command(gatk_cmd, log, profile, name='haplotype_caller')
    bgzip = Command(bgzip_cmd, log, profile, name='bgzip')
    tabix = Command(tabix_cmd, log, profile, name='tabix')

    gatk.add_namespace(profile.programs['gatk4'])
    bgzip.add_namespace(profile.programs['tabix'])
    tabix.add_namespace(profile.programs['tabix'])

    gatk.add_input(genome, 'recursive')
    gatk.add_input(args.input)
    tabix.add_input(output)
    tabix.add_output('%s.tbi' % output)

    log.log.info('Open file %s in write mode' % output)
    with open(output, 'wt') as output_file:
        bgzip.pipe_in(gatk)
        bgzip.stdout = output_file
        bgzip.run()
        bgzip.close()

    tabix.run()
    tabix.close()
