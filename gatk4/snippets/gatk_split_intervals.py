##
# cSpell:word pype, gatk, ncpu, nargs, exome

import os
from pype.misc import bases_format
from pype.process import Command


def requirements():
    return({'ncpu': 1, 'time': '48:00:00', 'mem': '4gb'})


def results(argv):
    try:
        output = argv['--out']
    except KeyError:
        output = argv['-o']
    output = os.path.realpath(output)
    try:
        n = argv['--scatter-count']
    except KeyError:
        n = argv['-n']
    except KeyError:
        n = 1
    try:
        exome = argv['--exome']
    except KeyError:
        exome = argv['-x']
    except KeyError:
        exome = False
    if exome:
        prefix = 'wxs'
    else:
        prefix = 'wgs'
    prefix = '%s_%i_parts' % (prefix, n)
    batch_file_out = os.path.join(output, 'batch_%s.txt' % prefix)
    return({'batch_file': batch_file_out})


def add_parser(subparsers, module_name):
    return subparsers.add_parser(
        module_name, help=(
            'Split intervals into sub-interval files'),
        add_help=False)


def gatk_split_intervals_args(parser, argv):
    parser.add_argument('-o', '--out', dest='out',
                        help='output folder for the scattered files',
                        required=True)
    parser.add_argument('-n', '--scatter-count', dest='scatter',
                        type=int, default=1,
                        help=('number of output interval files to split '
                              'into  Default value: 1'))
    parser.add_argument('-x', '--exome', dest='exome',
                        action="store_true",
                        help=('flag to use when analysing exon sequencing '
                              'regions'))
    return parser.parse_args(argv)


def gatk_split_intervals(subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = gatk_split_intervals_args(add_parser(
        subparsers, module_name), argv)

    build = profile.genome_build
    log.log.info('Use genome build %s' % build)
    genome = profile.files['genome_fa']
    if args.exome:
        intervals = profile.files['wxs_regions']
        prefix = 'wxs'
    else:
        intervals = profile.files['wgs_regions']
        prefix = 'wgs'

    prefix = '%s_%i_parts' % (prefix, args.scatter)

    intervals_list = list()
    for i in range(args.scatter):
        intervals_list.append(os.path.join(os.path.realpath(
            args.out), '%04.i-scattered.interval_list' % i))

    req = requirements()
    try:
        mem = int(bases_format(req['mem']) / (1000 * 1000))
    except KeyError:
        mem = 4000
    # remove 500Mb memory to run the command
    mem = mem - 500
    log.log.info('Set Java memory limit to %im' % mem)

    gatk_cmd = ('gatk --java-options "-Xmx%(memory)im" '
                'SplitIntervals '
                '-R %(genome)s -L %(intervals)s '
                '-O %(output)s --scatter-count %(scatter)i') % {
                    'memory': mem,
                    'genome': genome,
                    'scatter': args.scatter,
                    'output': args.out,
                    'intervals': intervals
                }

    gatk = Command(gatk_cmd, log, profile, name='split_intervals')
    gatk.add_namespace(profile.programs['gatk4'])
    gatk.add_input(intervals)
    gatk.add_output(args.out)

    gatk.run()
    gatk.close()
    batch_file_out = os.path.join(args.out, 'batch_%s.txt' % prefix)
    log.log.info('Prepare batch file %s' % batch_file_out)
    parts_counts = 0
    for result in os.listdir(args.out):
        result = os.path.join(os.path.realpath(args.out), result)
        if os.path.isfile(result):
            if result.endswith('scattered.interval_list'):
                parts_counts += 1
                if result not in intervals_list:
                    raise Exception('Unexpected interval file %s' % result)
    if parts_counts is not args.scatter:
        raise Exception('Wrong number of items, expected %i, '
                        'counted %i' % (args.scatter, parts_counts))
    with open(batch_file_out, 'wt') as batch_out:
        batch_out.write('--intervals\n')
        for intervals_file in intervals_list:
            log.log.info('Insert %s in batch file' % intervals_file)
            batch_out.write('%s\n' % intervals_file)
