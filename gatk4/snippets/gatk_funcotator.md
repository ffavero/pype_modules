
 [//]: # ( cSpell:word funcotator, gatk, ncpu, germline, exome )

# Run annotation with Funcotator

Funcotator from GATK

## description

Annotation with GATK funcotator

## requirements

```yaml
ncpu: 1
time: '24:00:00'
mem: 8gb
```

## results

```python
@/usr/bin/env python3, json

import os
import json

output = '%(output)s'

res = {'vcf': output}

print(json.dumps(res))
```

## name

```bash
@/bin/sh

output=`basename %(output)s`

echo "funcotator_$output"
```

## arguments

1. input/i
    - help: Input VCF file
    - type: str
    - required: true
2. output/o
    - help: Output annotated VCF file
    - type: str
    - required: true
3. germline
    - help: Flag setting the annotation callable regions
            and the running mode for exome
    - action: store_true

## snippet

> _input_: input profile_genome_fa
> profile_funcotator_germline profile_funcotator_somatic
> _output_: output

```bash
@/bin/sh, funcotator, namespace=gatk4

output=%(output)s
input=%(input)s
germline=%(germline)s
genome=%(profile_genome_fa)s
build_version=%(profile_genome_build)s

if [[ $germline == "True" ]]; then
    source_dir=%(profile_funcotator_germline)s
else
    source_dir=%(profile_funcotator_somatic)s
fi

gatk --java-options "-Xmx%(requirements_mem)im" \
     Funcotator \
     --variant $input \
     --reference $genome \
     --ref-version $build_version \
     --data-sources-path $source_dir \
     --output $output \
     --lenient \
     --output-file-format VCF

```
