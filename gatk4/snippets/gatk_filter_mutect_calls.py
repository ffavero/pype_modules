##
# cSpell:word pype, ncpu, gatk, mutect snvs

import os
import re
from pype.misc import bases_format
from pype.process import Command


def requirements():
    return({'ncpu': 1, 'time': '48:00:00', 'mem': '8gb'})


def results(argv):
    try:
        output = argv['--out']
    except KeyError:
        output = argv['-o']
    stats = re.sub('.gz$', '', output)
    stats = re.sub('.vcf$', '', output)
    stats = re.sub('.bcf$', '', output)
    stats = '%s_filter_stats.txt' % stats
    return({
        'vcf': output,
        'stats': stats
    })


def friendly_name(argv):
    try:
        rec_name = argv['--out']
    except KeyError:
        rec_name = argv['-o']
    rec_basename = os.path.basename(rec_name)
    return('%s_%s' % ('gatk_filter_mutect', rec_basename))


def add_parser(subparsers, module_name):
    return subparsers.add_parser(
        module_name, help=(
            'Filter somatic SNVs and indels called by Mutect2'),
        add_help=False)


def gatk_filter_mutect_calls_args(parser, argv):
    parser.add_argument('-i', '--input', dest='input',
                        help='input raw VCF file', required=True)
    parser.add_argument('-o', '--out', dest='out',
                        help='output filtered VCF file',
                        required=True)
    parser.add_argument('-m', '--mutect-stats', dest='mutect_stats',
                        help='the Mutect stats file output by Mutect2')
    parser.add_argument('-c', '--contamination', dest='contamination',
                        help='tables containing contamination information')
    parser.add_argument('-s', '--maf-segments', dest='maf_segments',
                        help='tables containing tumor segments')
    parser.add_argument('-a', '--orientation-model', dest='orientation_model',
                        help=('tar.gz files containing prior artifact '
                              'probabilities for the read orientation '
                              'filter model'))

    return parser.parse_args(argv)


def gatk_filter_mutect_calls(subparsers, module_name,
                             argv, profile, log):

    log.log.info('Process snippets arguments')
    args = gatk_filter_mutect_calls_args(add_parser(
        subparsers, module_name), argv)

    build = profile.genome_build
    log.log.info('Use genome build %s' % build)

    genome = profile.files['genome_fa']
    log.log.info('Use genome reference %s' % genome)

    output = args.out
    stats = re.sub('.gz$', '', output)
    stats = re.sub('.vcf$', '', output)
    stats = re.sub('.bcf$', '', output)
    stats = '%s_filter_stats.txt' % stats

    req = requirements()
    try:
        mem = int(bases_format(req['mem']) / (1000 * 1000))
    except KeyError:
        mem = 4000
    # remove 500Mb memory to run the command
    mem = mem - 500
    log.log.info('Set Java memory limit to %im' % mem)

    log.log.info('Preparing gatk command line')
    gatk_cmd = ('gatk --java-options "-Xmx%(memory)im" '
                'FilterMutectCalls '
                '-R  %(genome)s '
                '-V %(input)s '
                '--filtering-stats %(stats)s '
                '-O %(output)s') % {
                    'memory': mem,
                    'genome': genome,
                    'input': args.input,
                    'output': output,
                    'stats': stats
                }

    if args.mutect_stats:
        gatk_cmd = '%s -stats %s' % (gatk_cmd, args.mutect_stats)
    if args.contamination:
        gatk_cmd = '%s --contamination-table %s' % (
            gatk_cmd, args.contamination)
    if args.maf_segments:
        gatk_cmd = '%s --tumor-segmentation %s' % (
            gatk_cmd, args.maf_segments)
    if args.orientation_model:
        gatk_cmd = '%s --ob-priors %s' % (gatk_cmd, args.orientation_model)

    gatk = Command(gatk_cmd, log, profile, name='calculate_contamination')

    gatk.add_namespace(profile.programs['gatk4'])

    gatk.add_input(args.input)
    gatk.add_input(genome)
    if args.mutect_stats:
        gatk.add_input(args.mutect_stats)
    if args.contamination:
        gatk.add_input(args.contamination)
    if args.maf_segments:
        gatk.add_input(args.maf_segments)
    if args.orientation_model:
        gatk.add_input(args.orientation_model)
    gatk.add_output(output)
    gatk.add_output(stats)

    gatk.run()
    gatk.close()
