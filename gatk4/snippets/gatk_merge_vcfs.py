##
# cSpell:word pype, gatk, ncpu, nargs, vcfs

import os
from pype.misc import bases_format
from pype.process import Command


def requirements():
    return({'ncpu': 1, 'time': '48:00:00', 'mem': '4gb'})


def results(argv):
    try:
        output = argv['--out']
    except KeyError:
        output = argv['-o']
    return({'vcf': output})


def friendly_name(argv):
    try:
        rec_name = argv['--out']
    except KeyError:
        rec_name = argv['-o']
    rec_basename = os.path.basename(rec_name)
    return('%s_%s' % ('gatk_merge_vcf', rec_basename))


def add_parser(subparsers, module_name):
    return subparsers.add_parser(
        module_name, help=(
            'Merge multiple VCFs files'),
        add_help=False)


def gatk_merge_vcfs_args(parser, argv):
    parser.add_argument('-i', '--input', dest='input', nargs='*',
                        help='input VCFs files', required=True)
    parser.add_argument('-o', '--out', dest='out',
                        help='output merged VCF file name',
                        required=True)
    parser.add_argument('-t', '--tmp', dest='tmp',
                        help='temporary folder',
                        default='/tmp')
    return parser.parse_args(argv)


def gatk_merge_vcfs(subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = gatk_merge_vcfs_args(add_parser(
        subparsers, module_name), argv)

    req = requirements()
    try:
        mem = int(bases_format(req['mem']) / (1000 * 1000))
    except KeyError:
        mem = 4000
    # remove 500Mb memory to run the command
    mem = mem - 500
    log.log.info('Set Java memory limit to %im' % mem)

    gatk_cmd = ('gatk --java-options "-Xmx%(memory)im" '
                'MergeVcfs '
                '--TMP_DIR %(tmp)s '
                '-O %(output)s') % {
                    'memory': mem,
                    'output': args.out,
                    'tmp': args.tmp
                }

    for input_vcf in args.input:
        gatk_cmd = '%s -I %s' % (gatk_cmd, input_vcf)

    gatk = Command(gatk_cmd, log, profile, name='merge_vcfs')
    gatk.add_namespace(profile.programs['gatk4'])
    for input_vcf in args.input:
        gatk.add_input(input_vcf)
    gatk.add_output(args.out)

    gatk.run()
    gatk.close()
