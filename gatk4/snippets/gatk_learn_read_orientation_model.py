##
# cSpell:word pype, gatk, ncpu, nargs

import os
from pype.misc import bases_format
from pype.process import Command


def requirements():
    return({'ncpu': 1, 'time': '48:00:00', 'mem': '8gb'})


def results(argv):
    try:
        output = argv['--out']
    except KeyError:
        output = argv['-o']
    return({'model': output})


def friendly_name(argv):
    try:
        rec_name = argv['--out']
    except KeyError:
        rec_name = argv['-o']
    rec_basename = os.path.basename(rec_name)
    return('%s_%s' % ('gatk_read_orientation', rec_basename))


def add_parser(subparsers, module_name):
    return subparsers.add_parser(
        module_name, help=(
            'Get the maximum likelihood estimates '
            'of artifact prior probabilities in the '
            'orientation bias mixture model filter'),
        add_help=False)


def learn_read_orientation_model_args(parser, argv):
    parser.add_argument('-i', '--input', dest='input', nargs='*',
                        help=('One or more .tar.gz containing outputs '
                              'of CollectF1R2Counts'), required=True)
    parser.add_argument('-o', '--out', dest='out',
                        help='Output table file', required=True)
    return parser.parse_args(argv)


def gatk_learn_read_orientation_model(subparsers, module_name,
                                      argv, profile, log):
    log.log.info('Process snippets arguments')
    args = learn_read_orientation_model_args(add_parser(
        subparsers, module_name), argv)

    output = args.out

    req = requirements()
    try:
        mem = int(bases_format(req['mem']) / (1000 * 1000))
    except KeyError:
        mem = 4000
    # remove 500Mb memory to run the command
    mem = mem - 500
    log.log.info('Set Java memory limit to %im' % mem)

    gatk_cmd = ('gatk --java-options "-Xmx%(memory)im" '
                'LearnReadOrientationModel '
                '-O %(output)s') % {
                    'memory': mem,
                    'output': output
                }
    for f1r2 in args.input:
        gatk_cmd = '%s -I %s' % (gatk_cmd, f1r2)
    gatk = Command(gatk_cmd, log, profile, name='read_orientation_model')
    gatk.add_namespace(profile.programs['gatk4'])
    for f1r2 in args.input:
        gatk.add_input(f1r2)
    gatk.add_output(output)

    gatk.run()
    gatk.close()
