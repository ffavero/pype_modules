##
# cSpell:word pype, ncpu, gatk, BQSR

import os
from pype.misc import bases_format
from pype.process import Command


def requirements():
    return({'ncpu': 1, 'time': '48:00:00', 'mem': '4gb'})


def results(argv):
    try:
        output = argv['--out']
    except KeyError:
        output = argv['-o']
    if '--intervals' in argv.keys() or '-l' in argv.keys():
        try:
            intervals = argv['--intervals']
        except KeyError:
            intervals = argv['-l']
    else:
        intervals = None
    if intervals == 'None':
        intervals = None
    if intervals is not None:
        if os.path.isfile(intervals):
            intervals_name = os.path.basename(intervals)
        else:
            intervals_name = intervals.replace(
                ':', '_').replace('-', '_')
        output_ext = output[-4:]
        if output_ext.startswith('.'):
            output = '%s_%s%s' % (
                output[0:-4], intervals_name, output_ext)
        else:
            output = '%s_%s' % (
                output, intervals_name)
        output = os.path.join(
            os.path.dirname(output), 'partials',
            os.path.basename(output))
    return({
        'report': output
    })


def friendly_name(argv):
    try:
        rec_name = argv['--out']
    except KeyError:
        rec_name = argv['-o']
    rec_basename = os.path.basename(rec_name)
    return('%s_%s' % ('gatk_BQSR', rec_basename))


def add_parser(subparsers, module_name):
    return subparsers.add_parser(
        module_name, help=(
            'First pass of the Base Quality Score Recalibration (BQSR)'),
        add_help=False)


def gatk_base_recalibrator_args(parser, argv):
    parser.add_argument('-i', '--input', dest='input',
                        help='input BAM file', required=True)
    parser.add_argument('-l', '--intervals', dest='intervals',
                        help='chromosome intervals(s)')
    parser.add_argument('-o', '--out', dest='out',
                        help='output recalibration report file name',
                        required=True)
    return parser.parse_args(argv)


def gatk_base_recalibrator(subparsers, module_name,
                           argv, profile, log):

    log.log.info('Process snippets arguments')
    args = gatk_base_recalibrator_args(add_parser(
        subparsers, module_name), argv)

    build = profile.genome_build
    log.log.info('Use genome build %s' % build)

    genome = profile.files['genome_fa']
    dbSNP = profile.files['dbSNP']
    known_indels = profile.files['known_indels']
    log.log.info('Use genome reference %s' % genome)
    log.log.info('Use known SNPs from %s' % dbSNP)
    log.log.info('Use known indels from %s' % known_indels)

    output = args.out
    intervals = args.intervals
    if intervals is not None:
        if os.path.isfile(intervals):
            intervals_name = os.path.basename(intervals)
        else:
            intervals_name = intervals.replace(
                ':', '_').replace('-', '_')
        output_ext = output[-4:]
        if output_ext.startswith('.'):
            output = '%s_%s%s' % (
                output[0:-4], intervals_name, output_ext)
        else:
            output = '%s_%s' % (
                output, intervals_name)
        output = os.path.join(
            os.path.dirname(output), 'partials',
            os.path.basename(output))

    req = requirements()
    try:
        mem = int(bases_format(req['mem']) / (1000 * 1000))
    except KeyError:
        mem = 4000
    # remove 500Mb memory to run the command
    mem = mem - 500
    log.log.info('Set Java memory limit to %im' % mem)

    log.log.info('Preparing gatk command line')
    gatk_cmd = ('gatk --java-options "-Xmx%(memory)im" '
                'BaseRecalibrator -R %(genome)s '
                '-I  %(input)s '
                '--use-original-qualities '
                '--known-sites %(dbSNP)s '
                '--known-sites %(indels)s '
                '-O %(output)s') % {
                    'memory': mem,
                    'genome': genome,
                    'input': args.input,
                    'output': output,
                    'dbSNP': dbSNP,
                    'indels': known_indels
                }
    if args.intervals:
        gatk_cmd = '%s -L %s' % (gatk_cmd, args.intervals)

    gatk = Command(gatk_cmd, log, profile, name='base_recalibrator')

    gatk.add_namespace(profile.programs['gatk4'])

    gatk.add_input(genome, 'recursive')
    gatk.add_input(args.input)
    gatk.add_output(output)

    gatk.run()
    gatk.close()
