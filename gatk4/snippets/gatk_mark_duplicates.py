##
# cSpell:word pype, gatk, ncpu, nargs, dups

import os
from pype.misc import bases_format
from pype.process import Command


def requirements():
    return({'ncpu': 1, 'time': '48:00:00', 'type': 'exclusive'})


def results(argv):
    try:
        output = argv['--out']
    except KeyError:
        output = argv['-o']
    return({
        'bam': output,
        'md5': '%s.md5' % output
    })


def friendly_name(argv):
    try:
        bam_name = argv['--out']
    except KeyError:
        bam_name = argv['-o']
    bam_basename = os.path.basename(bam_name)
    return('%s_%s' % ('gatk_mark_dups', bam_basename))


def add_parser(subparsers, module_name):
    return subparsers.add_parser(
        module_name, help='Mark duplicate reads in a BAM file',
        add_help=False)


def gatk_mark_duplicates_args(parser, argv):
    parser.add_argument('-i', '--input', dest='input', nargs='*',
                        help='input BAM files', required=True)
    parser.add_argument('-o', '--out', dest='out',
                        help='output BAM file', required=True)
    parser.add_argument('-m', '--metrics', dest='metrics',
                        help='output metrics file', required=True)
    parser.add_argument('-t', '--tmp', dest='tmp',
                        help='temporary folder',
                        default='/tmp')
    return parser.parse_args(argv)


def gatk_mark_duplicates(subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = gatk_mark_duplicates_args(add_parser(
        subparsers, module_name), argv)

    req = requirements()
    try:
        mem = int(bases_format(req['mem']) / (1000 * 1000))
    except KeyError:
        mem = 4000
    # remove 500Mb memory to run the command
    mem = mem - 500
    log.log.info('Set Java memory limit to %im' % mem)

    log.log.info('Preparing gatk command line')
    gatk_cmd = ('gatk --java-options "-Xmx%(memory)im" '
                'MarkDuplicates '
                '--VALIDATION_STRINGENCY SILENT '
                '--OPTICAL_DUPLICATE_PIXEL_DISTANCE 2500 '
                '--ASSUME_SORT_ORDER "coordinate" '
                '--CREATE_MD5_FILE true '
                '--CREATE_INDEX true '
                '--METRICS_FILE %(metrics)s '
                '--TMP_DIR %(tmp)s '
                '--OUTPUT %(output)s') % {
                    'memory': mem,
                    'output': args.out,
                    'metrics': args.metrics,
                    'tmp': args.tmp
                }
    for bam in args.input:
        gatk_cmd = '%s --INPUT %s' % (gatk_cmd, bam)

    gatk = Command(gatk_cmd, log, profile, name='mark_duplicate')

    gatk.add_namespace(profile.programs['gatk4'])
    for bam in args.input:
        gatk.add_input(bam)
    gatk.add_output(args.out)
    gatk.add_output(args.metrics)

    gatk.run()
    gatk.close()
