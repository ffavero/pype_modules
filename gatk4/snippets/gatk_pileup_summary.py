##
# cSpell:word pype, gatk, ncpu, nargs

import os
from pype.misc import bases_format
from pype.process import Command


def requirements():
    return({'ncpu': 1, 'time': '48:00:00', 'mem': '16gb'})


def results(argv):
    try:
        output = argv['--out']
    except KeyError:
        output = argv['-o']
    if '--intervals' in argv.keys() or '-l' in argv.keys():
        try:
            intervals = argv['--intervals']
        except KeyError:
            intervals = argv['-l']
    else:
        intervals = None
    if intervals == 'None':
        intervals = None
    if intervals is not None:
        if os.path.isfile(intervals):
            intervals_name = os.path.basename(intervals)
        else:
            intervals_name = intervals.replace(
                ':', '_').replace('-', '_')
        output_ext = output[-4:]
        if output_ext.startswith('.'):
            output = '%s_%s%s' % (
                output[0:-4], intervals_name, output_ext)
        else:
            output = '%s_%s' % (
                output, intervals_name)
        output = os.path.join(
            os.path.dirname(output), 'partials',
            os.path.basename(output))
    return({'table': output})


def friendly_name(argv):
    res = results(argv)
    rec_basename = os.path.basename(res['table'])
    return('%s_%s' % ('gatk_pileup', rec_basename))


def add_parser(subparsers, module_name):
    return subparsers.add_parser(
        module_name, help=(
            'Tabulates pileup metrics for inferring contamination'),
        add_help=False)


def gatk_pileup_summary_args(parser, argv):
    parser.add_argument('-i', '--input', dest='input',
                        help='input bam file', required=True)
    parser.add_argument('-o', '--out', dest='out',
                        help='Output table file', required=True)
    parser.add_argument('-l', '--intervals', dest='intervals',
                        help=('chromosome location(s) where to '
                              'run GetPileupSummaries'))
    return parser.parse_args(argv)


def gatk_pileup_summary(subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = gatk_pileup_summary_args(add_parser(
        subparsers, module_name), argv)

    build = profile.genome_build
    log.log.info('Use genome build %s' % build)
    snp = profile.files['common_ExAC']
    log.log.info('Use snp collection at %s' % snp)

    output = args.out
    intervals = args.intervals
    if intervals is not None:
        if os.path.isfile(intervals):
            intervals_name = os.path.basename(intervals)
        else:
            intervals_name = intervals.replace(
                ':', '_').replace('-', '_')
        output_ext = output[-4:]
        if output_ext.startswith('.'):
            output = '%s_%s%s' % (
                output[0:-4], intervals_name, output_ext)
        else:
            output = '%s_%s' % (
                output, intervals_name)
        output = os.path.join(
            os.path.dirname(output), 'partials',
            os.path.basename(output))

    req = requirements()
    try:
        mem = int(bases_format(req['mem']) / (1000 * 1000))
    except KeyError:
        mem = 4000
    # remove 500Mb memory to run the command
    mem = mem - 500
    log.log.info('Set Java memory limit to %im' % mem)

    gatk_cmd = ('gatk --java-options "-Xmx%(memory)im" '
                'GetPileupSummaries '
                '-I %(input)s -O %(output)s') % {
                    'memory': mem,
                    'input': args.input,
                    'output': output
                }
    if args.intervals:
        gatk_cmd = ('%s  --interval-set-rule INTERSECTION '
                    '-L %s' % (gatk_cmd, args.intervals))
    gatk_cmd = '%(cmd)s -V %(snp)s -L %(snp)s' % {
        'cmd': gatk_cmd,
        'snp': snp}
    gatk = Command(gatk_cmd, log, profile, name='pileup_summary')
    gatk.add_namespace(profile.programs['gatk4'])
    gatk.add_input(args.input)
    gatk.add_output(output)

    gatk.run()
    gatk.close()
