
##
# cSpell:word pype, ncpu, gatk, BSQR, BQSR, quals

import os
import re
from pype.misc import bases_format
from pype.process import Command


def requirements():
    return({'ncpu': 1, 'time': '48:00:00', 'mem': '4gb'})


def results(argv):
    try:
        output = argv['--out']
    except KeyError:
        output = argv['-o']
    if '--intervals' in argv.keys() or '-l' in argv.keys():
        try:
            intervals = argv['--intervals']
        except KeyError:
            intervals = argv['-l']
    else:
        intervals = None
    if intervals == 'None':
        intervals = None
    if intervals is not None:
        if os.path.isfile(intervals):
            intervals_name = os.path.basename(intervals)
        else:
            intervals_name = intervals.replace(
                ':', '_').replace('-', '_')
        output_ext = output[-4:]
        if output_ext.startswith('.'):
            output = '%s_%s%s' % (
                output[0:-4], intervals_name, output_ext)
        else:
            output = '%s_%s' % (
                output, intervals_name)
        output = os.path.join(
            os.path.dirname(output), 'partials',
            os.path.basename(output))
    return({
        'bam': output,
        'bai': re.sub('.bam$', '.bai', output),
        'md5': '%s.md5' % output
    })


def friendly_name(argv):
    try:
        bam_name = argv['--out']
    except KeyError:
        bam_name = argv['-o']
    bam_basename = os.path.basename(bam_name)
    return('%s_%s' % ('gatk_apply_BQSR', bam_basename))


def add_parser(subparsers, module_name):
    return subparsers.add_parser(
        module_name, help=(
            'Apply Base Quality Score Recalibration'),
        add_help=False)


def gatk_apply_BSQR_args(parser, argv):
    parser.add_argument('-i', '--input', dest='input',
                        help='input BAM file', required=True)
    parser.add_argument('-r', '--bsqr', dest='bsqr',
                        help='input BSQR report', required=True)
    parser.add_argument('-l', '--intervals', dest='intervals',
                        help='chromosome intervals(s)')
    parser.add_argument('-o', '--out', dest='out',
                        help='output BAM file',
                        required=True)
    return parser.parse_args(argv)


def gatk_apply_BSQR(subparsers, module_name,
                    argv, profile, log):

    log.log.info('Process snippets arguments')
    args = gatk_apply_BSQR_args(add_parser(
        subparsers, module_name), argv)

    build = profile.genome_build
    log.log.info('Use genome build %s' % build)

    genome = profile.files['genome_fa']
    log.log.info('Use genome reference %s' % genome)

    output = args.out
    intervals = args.intervals
    if intervals is not None:
        if os.path.isfile(intervals):
            intervals_name = os.path.basename(intervals)
        else:
            intervals_name = intervals.replace(
                ':', '_').replace('-', '_')
        output_ext = output[-4:]
        if output_ext.startswith('.'):
            output = '%s_%s%s' % (
                output[0:-4], intervals_name, output_ext)
        else:
            output = '%s_%s' % (
                output, intervals_name)
        output = os.path.join(
            os.path.dirname(output), 'partials',
            os.path.basename(output))

    req = requirements()
    try:
        mem = int(bases_format(req['mem']) / (1000 * 1000))
    except KeyError:
        mem = 4000
    # remove 500Mb memory to run the command
    mem = mem - 500
    log.log.info('Set Java memory limit to %im' % mem)

    log.log.info('Preparing gatk command line')
    gatk_cmd = ('gatk --java-options "-Xmx%(memory)im" '
                'ApplyBQSR -R %(genome)s '
                '-I  %(input)s '
                '--static-quantized-quals 10 '
                '--static-quantized-quals 20 '
                '--static-quantized-quals 30 '
                '--emit-original-quals '
                '--add-output-sam-program-record '
                '--create-output-bam-md5 '
                '--create-output-bam-index '
                '--use-original-qualities '
                '-bqsr %(bsqr_report)s '
                '-O %(output)s') % {
                    'memory': mem,
                    'genome': genome,
                    'input': args.input,
                    'output': output,
                    'bsqr_report': args.bsqr
                }

    if args.intervals:
        gatk_cmd = '%s -L %s' % (gatk_cmd, args.intervals)

    gatk = Command(gatk_cmd, log, profile, name='apply_base_recalibrator')

    gatk.add_namespace(profile.programs['gatk4'])

    gatk.add_input(genome, 'recursive')
    gatk.add_input(args.input)
    gatk.add_output(output)

    gatk.run()
    gatk.close()

    output_index = '%s.bai' % os.path.splitext(output)[0]
    symlink_bai = '%s.bai' % output

    log.log.info(
        'Symlink bam index %s to %s' % (output_index, symlink_bai))
    os.symlink(output_index, symlink_bai)