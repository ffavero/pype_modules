##
# cSpell:word pype, gatk, ncpu, nargs, BQSR

import os
import re
from pype.misc import bases_format
from pype.process import Command


def requirements():
    return({'ncpu': 1, 'time': '48:00:00', 'mem': '4gb'})


def results(argv):
    try:
        output = argv['--out']
    except KeyError:
        output = argv['-o']
    return({
        'report': output
    })


def friendly_name(argv):
    try:
        rec_name = argv['--out']
    except KeyError:
        rec_name = argv['-o']
    rec_basename = os.path.basename(rec_name)
    return('%s_%s' % ('gatk_gather_pileup', rec_basename))


def add_parser(subparsers, module_name):
    return subparsers.add_parser(
        module_name, help=(
            'Merge multiple BQSR into one report'),
        add_help=False)


def gatk_gather_pileup_summaries_args(parser, argv):
    parser.add_argument('-i', '--input', dest='input', nargs='*',
                        help='scattered pileup summary tables', required=True)
    parser.add_argument('-o', '--out', dest='out',
                        help='output pileup summary file name',
                        required=True)
    parser.add_argument('-t', '--tmp', dest='tmp',
                        help='temporary folder',
                        default='/tmp')
    return parser.parse_args(argv)


def gatk_gather_pileup_summaries(subparsers, module_name,
                                 argv, profile, log):

    log.log.info('Process snippets arguments')
    args = gatk_gather_pileup_summaries_args(add_parser(
        subparsers, module_name), argv)

    output = args.out
    genome = profile.files['genome_fa']
    genome_dict = re.sub('.fa', '.dict', genome)
    log.log.info('Use genome reference %s' % genome)

    req = requirements()
    try:
        mem = int(bases_format(req['mem']) / (1000 * 1000))
    except KeyError:
        mem = 4000
    # remove 500Mb memory to run the command
    mem = mem - 500
    log.log.info('Set Java memory limit to %im' % mem)

    log.log.info('Preparing gatk command line')
    gatk_cmd = ('gatk --java-options "-Xmx%(memory)im" '
                'GatherPileupSummaries '
                '-O %(output)s --tmp-dir %(tmp_dir)s '
                '--sequence-dictionary %(genome_dict)s') % {
                    'memory': mem,
                    'output': output,
                    'tmp_dir': args.tmp,
                    'genome_dict': genome_dict
                }
    for summary in args.input:
        gatk_cmd = '%s -I %s' % (gatk_cmd, summary)

    gatk = Command(gatk_cmd, log, profile, name='gather_pileup_summaries')

    gatk.add_namespace(profile.programs['gatk4'])
    for summary in args.input:
        gatk.add_input(summary)
    gatk.add_input(genome_dict)
    gatk.add_output(output)

    gatk.run()
    gatk.close()
