##
# cSpell:word pype, binfmisc, fastq, ncpu, gatk, demultiplex, bamsort

import os
from datetime import datetime
from pype.process import Command
from pype.binfmisc import fastq_name_info
from pype.misc import generate_uid, bases_format


def requirements():
    return({'ncpu': 16, 'mem': '64gb', 'time': '5:00:00:00'})


def results(argv):
    try:
        output = argv['--out']
    except KeyError:
        output = argv['-o']
    return({
        'bam': output,
        'md5': '%s.md5' % output
    })


def friendly_name(argv):
    try:
        sample_name = argv['--sample-name']
    except KeyError:
        sample_name = argv['-s']
    sample_name = ''.join([i.replace('/', '_') for i in sample_name])
    return('%s_%s' % ('gatk_bwa_mem', sample_name))


def add_parser(subparsers, module_name):
    return subparsers.add_parser(
        module_name, help='Align fastq or uBAM to a reference',
        add_help=False)


def gatk_bwa_mem_args(parser, argv):
    parser.add_argument('-1', '--f1', dest='f1', required=True,
                        help='first mate fastq file')
    parser.add_argument('-s', '--sample-name', dest='name', required=True,
                        help='sample name')
    parser.add_argument('-o', '--out', dest='out',
                        help='output BAM file', required=True)
    parser.add_argument('-2', '--f2', dest='f2',
                        help='second mate fastq file')
    parser.add_argument('-t', '--tmp', dest='tmp',
                        help='temporary folder',
                        default='/tmp')
    parser.add_argument('-R', '--read-group', dest='rg',
                        help=('read group name. If not present will attempt '
                              'to automatically generate reading the fastq'))
    parser.add_argument('--machine-id', dest='machine',
                        help='sequencing machine identifier')
    parser.add_argument('--flowcell-id', dest='flowcell',
                        help='flowcell identifier')
    parser.add_argument('--lane-number', dest='lane',
                        help='lane number')
    parser.add_argument('--index-id', dest='index',
                        help='demultiplex index identifier')
    return parser.parse_args(argv)


def gatk_bwa_mem(subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = gatk_bwa_mem_args(add_parser(
        subparsers, module_name), argv)

    build = profile.genome_build
    log.log.info('Use genome build %s' % build)

    genome = profile.files['genome_fa']
    log.log.info('Use genome reference %s' % genome)

    log.log.info('Generate the reads group tag (@RG):')
    rg_info = {
        'machine_id': args.machine,
        'flowcell_id': args.flowcell,
        'lane': args.lane,
        'mate': None,
        'index': args.index
    }
    fastq_info = fastq_name_info(args.f1)

    for key in fastq_info.keys():
        if rg_info[key] is None:
            rg_info[key] = fastq_info[key]
    if rg_info['flowcell_id'] is None:
        rg_info['flowcell_id'] = generate_uid(7)[-7:]

    sample_uuid = '%s.%s.%s.%s' % (
        rg_info['machine_id'], rg_info['flowcell_id'],
        rg_info['lane'], args.name)
    rg_group = {
        'ID': sample_uuid,
        'SM': args.name,
        'PL': 'illumina',
        'PU': sample_uuid,
        'LB': None,
        'PI': None,
        'DT': datetime.now().strftime('%Y-%m-%d'),
        'CN': None
    }
    header = '@RG'
    for key in rg_group.keys():
        if rg_group[key] is not None:
            header = '%s\\t%s:%s' % (
                header, key, rg_group[key]
            )
    log.log.info('Set read group as %s' % header)
    if args.f2 is None:
        log.log.info('Prepare single end alignment')
        fastq_files = [args.f1]
    else:
        log.log.info('Prepare paired end alignment')
        fastq_files = [args.f1, args.f2]
    log.log.info('Prepare bwa mem command line')

    bwa_cmd = (
        'bwa mem -t 16 -T 0 '
        '-R "%(header)s" %(genome)s %(fastq)s') % {
            'header': header,
            'genome': genome,
            'fastq': ' '.join(fastq_files)
        }
    random_str = generate_uid()

    req = requirements()
    try:
        mem = int(bases_format(req['mem']) / (1000 * 1000))
    except KeyError:
        mem = 4000
    # remove 500Mb memory to run the command
    mem = mem - 500
    log.log.info('Set Java memory limit to %im' % mem)

    gatk_sort_cmd = (
        'gatk --java-options "-Xmx%(memory)im" '
        'SortSam '
        '--INPUT %(input)s '
        '--OUTPUT %(output)s '
        '--SORT_ORDER "coordinate" '
        '--CREATE_INDEX false '
        '--CREATE_MD5_FILE false '
        '--COMPRESSION_LEVEL 0 '
        '--TMP_DIR %(tmp)s'
    ) % {
        'memory': mem,
        'input': '/dev/stdin',
        'output': '/dev/stdout',
        'tmp': args.tmp
    }

    gatk_fix_tags_cmd = (
        'gatk SetNmMdAndUqTags '
        '--INPUT /dev/stdin '
        '--OUTPUT %(output)s '
        '--REFERENCE_SEQUENCE %(genome)s '
        '--CREATE_INDEX true '
        '--CREATE_MD5_FILE true  '
        '--COMPRESSION_LEVEL 5'
    ) % {'output': args.out, 'genome': genome}

    bwa = Command(bwa_cmd, log, profile, name='bwa')
    sort = Command(gatk_sort_cmd, log, profile, name='sort')
    fixtags = Command(gatk_fix_tags_cmd, log, profile, name='fix_tags')

    bwa.add_namespace(profile.programs['bwa'])
    sort.add_namespace(profile.programs['gatk4'])
    fixtags.add_namespace(profile.programs['gatk4'])

    for fastq in fastq_files:
        bwa.add_input(fastq)
    bwa.add_input(genome, 'recursive')
    sort.add_output(
        os.path.join(args.tmp, 'bamsort_%s' % random_str))

    fixtags.add_output(args.out)
    fixtags.add_output('%s.bai' % args.out)
    fixtags.add_output('%s.md5' % args.out)

    sort.pipe_in(bwa)
    fixtags.pipe_in(sort)
    fixtags.run()
    fixtags.close()
