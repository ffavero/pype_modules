##
# cSpell:word pype, gatk, ncpu, nargs

import os
from pype.misc import bases_format
from pype.process import Command


def requirements():
    return({'ncpu': 1, 'time': '48:00:00', 'mem': '4gb'})


def results(argv):
    try:
        output = argv['--out']
    except KeyError:
        output = argv['-o']
    return({
        'bam': output,
        'md5': '%s.md5' % output
    })


def friendly_name(argv):
    try:
        rec_name = argv['--out']
    except KeyError:
        rec_name = argv['-o']
    rec_basename = os.path.basename(rec_name)
    return('%s_%s' % ('gatk_gather_bam', rec_basename))


def add_parser(subparsers, module_name):
    return subparsers.add_parser(
        module_name, help='Gather scattered BAM files',
        add_help=False)


def gatk_gather_bam_files_args(parser, argv):
    parser.add_argument('-i', '--input', dest='input', nargs='*',
                        help='input BAM files', required=True)
    parser.add_argument('-o', '--out', dest='out',
                        help='output BAM file', required=True)
    parser.add_argument('-t', '--tmp', dest='tmp',
                        help='temporary folder',
                        default='/tmp')
    return parser.parse_args(argv)


def gatk_gather_bam_files(subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = gatk_gather_bam_files_args(add_parser(
        subparsers, module_name), argv)

    req = requirements()
    try:
        mem = int(bases_format(req['mem']) / (1000 * 1000))
    except KeyError:
        mem = 4000
    # remove 500Mb memory to run the command
    mem = mem - 500
    log.log.info('Set Java memory limit to %im' % mem)

    log.log.info('Preparing gatk command line')
    gatk_cmd = ('gatk --java-options "-Xmx%(memory)im" '
                'GatherBamFiles '
                '--CREATE_MD5_FILE true '
                '--CREATE_INDEX true '
                '--TMP_DIR %(tmp)s '
                '--OUTPUT %(output)s') % {
                    'memory': mem,
                    'output': args.out,
                    'tmp': args.tmp
                }
    for bam in args.input:
        gatk_cmd = '%s --INPUT %s' % (gatk_cmd, bam)

    gatk = Command(gatk_cmd, log, profile, name='gather_bam_files')

    gatk.add_namespace(profile.programs['gatk4'])
    for bam in args.input:
        gatk.add_input(bam)
    gatk.add_output(args.out)

    gatk.run()
    gatk.close()
