##
# cSpell:word pype, gatk, ncpu, nargs, mutect, gnomad

import os
import re
from pype.misc import bases_format
from pype.process import Command


def requirements():
    return({'ncpu': 2, 'time': '48:00:00', 'mem': '16gb'})


def results(argv):
    try:
        output = argv['--out']
    except KeyError:
        output = argv['-o']
    if '--intervals' in argv.keys() or '-l' in argv.keys():
        try:
            intervals = argv['--intervals']
        except KeyError:
            intervals = argv['-l']
    else:
        intervals = None
    if intervals == 'None':
        intervals = None
    output = '%s' % re.sub('.gz$', '', output)
    output = '%s' % re.sub('.vcf$', '', output)
    output = '%s' % re.sub('.bcf$', '', output)

    if intervals is not None:
        if os.path.isfile(intervals):
            intervals_name = os.path.basename(intervals)
        else:
            intervals_name = intervals.replace(
                ':', '_').replace('-', '_')
        output = '%s_%s' % (
            output, intervals_name)
        output = os.path.join(
            os.path.dirname(output), 'partials',
            os.path.basename(output))

    f1r2 = '%s.f1r2.tar.gz' % output
    output = '%s.vcf.gz' % output
    stats = '%s.stats' % output

    return({
        'vcf': output,
        'idx': '%s.tbi' % output,
        'stats': stats,
        'f1r2': f1r2
    })


def friendly_name(argv):
    res = results(argv)
    rec_basename = os.path.basename(res['vcf'])
    return('%s_%s' % ('gatk_mutect2', rec_basename))


def add_parser(subparsers, module_name):
    return subparsers.add_parser(
        module_name, help='Call somatic variant with gatk MuTect2',
        add_help=False)


def gatk_mutect2_args(parser, argv):
    parser.add_argument('-i', '--input', dest='input', nargs='*',
                        action='append', help=(
                            'input normal and tumor BAM files'),
                        required=True)
    parser.add_argument('-n', '--normal', dest='normal', nargs='*',
                        help='normal sample names in the normal BAM files')
    parser.add_argument('-l', '--intervals', dest='intervals',
                        help='chromosome location(s) where to run Mutect2')
    parser.add_argument('-m', '--min-mapping-quality', dest='min_map',
                        help=('Exclude alignments from analysis if '
                              'they have a mapping quality less than MIN_MAP'),
                        type=int, default=30)
    parser.add_argument('-q', '--min-base-quality', dest='min_qual',
                        help=('Exclude alleles from analysis if their '
                              'supporting base quality is less than MIN_QUAL'),
                        type=int, default=20)
    parser.add_argument('-o', '--out', dest='out',
                        help='Output vcf file', required=True)
    return parser.parse_args(argv)


def gatk_mutect2(subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = gatk_mutect2_args(add_parser(
        subparsers, module_name), argv)

    build = profile.genome_build
    log.log.info('Use genome build %s' % build)

    genome = profile.files['genome_fa']
    log.log.info('Use genome reference %s' % genome)

    snp = profile.files['gnomAD']
    pon = profile.files['mutect_pon']

    log.log.info('Use known variants %s' % snp)
    output = '%s' % re.sub(r'\.gz$', '', args.out)
    output = '%s' % re.sub(r'\.vcf$', '', output)
    output = '%s' % re.sub(r'\.bcf$', '', output)

    intervals = args.intervals
    if intervals is not None:
        if os.path.isfile(intervals):
            intervals_name = os.path.basename(intervals)
        else:
            intervals_name = intervals.replace(
                ':', '_').replace('-', '_')
        output = '%s_%s' % (
            output, intervals_name)
        output = os.path.join(
            os.path.dirname(output), 'partials',
            os.path.basename(output))
    f1r2 = '%s.f1r2.tar.gz' % output
    output = '%s.vcf.gz' % output
    bam_input = list()
    for bam_list in args.input:
        for bam in bam_list:
            if bam not in bam_input:
                bam_input.append(bam)

    req = requirements()
    try:
        mem = int(bases_format(req['mem']) / (1000 * 1000))
    except KeyError:
        mem = 4000
    # remove 500Mb memory to run the command
    mem = mem - 500
    log.log.info('Set Java memory limit to %im' % mem)

    log.log.info('Preparing gatk command line')
    gatk_cmd = ('gatk --java-options "-Xmx%(memory)im" '
                'Mutect2 -R %(genome)s '
                '--germline-resource %(snp)s '
                '--minimum-mapping-quality %(min_map)s '
                '--min-base-quality-score %(min_qual)s '
                '-pon %(pon)s '
                '-O %(output)s --f1r2-tar-gz %(f1r2)s') % {
                    'memory': mem,
                    'genome': genome,
                    'snp': snp,
                    'pon': pon,
                    'min_map': args.min_map,
                    'min_qual': args.min_qual,
                    'output': output,
                    'f1r2': f1r2
                }
    for bam in bam_input:
        gatk_cmd = '%s -I %s' % (gatk_cmd, bam)
    if args.normal:
        for normal in args.normal:
            gatk_cmd = '%s -normal %s' % (gatk_cmd, normal)
    if args.intervals:
        gatk_cmd = '%s -L %s' % (gatk_cmd, args.intervals)

    gatk = Command(gatk_cmd, log, profile, name='MuTect2')

    gatk.add_namespace(profile.programs['gatk4'])

    gatk.add_input(genome, 'recursive')
    for bam in args.input:
        gatk.add_input(bam)
    gatk.add_output(output)
    gatk.add_output('%s.stats' % output)
    gatk.add_output(f1r2)

    gatk.run()
    gatk.close()
