# Extract SV Footprint signatures

Compute the complex SV footprint signature
from the RGS file.

## description

Extract SV Footprint signatures.

## requirements

```yaml
ncpu: 1
time: '12:00:00'
mem: 4gb
```

## results

```python
/bin/env python3, json
import json

output_arg = '%(classification)s'

res = {'classification': output_arg}

print(json.dumps(res))
```

## arguments

1. sample/i

    - help: Sample name/ID
    - type: str
    - required: true

2. cellularity/c

    - help: Cellularity value (fraction from 0 to 1)
    - type: float
    - required: true

3. ploidy/p

    - help: Ploidy value (eg 2 for diploid)
    - type: float
    - required: true

4. cluster

    - help: Cluster SV bedpe file
    - type: str
    - required: true

5. breakpoints

    - help: Adjusted SV breakpoints
    - type: str
    - required: true

6. rg

    - help: RG copy number
    - type: str
    - required: true

7. classification

    - help: SV footprint classification file
    - type: str
    - required: true


## snippet

> _input_: breakpoints rg cluster

> _output_: results_classification

```bash
@/bin/sh, conversion_cov, namespace=csvc

set -e

sample='%(sample)s'
ploidy='%(ploidy)s'
acf='%(cellularity)s'
breakpoints='%(breakpoints)s'
cluster='%(cluster)s'
rg_cns='%(rg)s'
classification='%(classification)s'

extract_footprint_analysis_stats.pl \
    -acf $acf -ploidy $ploidy \
    -sample_name $sample \
    -sv_clusters_file $cluster \
    -sv_classification $classification \
    $breakpoints $rg_cns

```
