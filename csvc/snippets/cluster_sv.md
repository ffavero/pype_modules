# cSVc cluster SV

Uses BRASS R code to cluster SV breakpoints

## description

Cluster SV

## requirements

```yaml
ncpu: 2
time: '12:00:00'
mem: 4gb
```

## results

```python
/bin/env python3, json
import json

input_arg = '%(input)s'

cluster_sv = input_arg + '.sv_clusters_and_footprints'
dist_pvals = input_arg + '.sv_distance_pvals'

res = {
    'clusters': cluster_sv,
    'dist_pvals': dist_pvals}

print(json.dumps(res))
```

## arguments

1. input/i

    - help: SV breakpoints file in bedpe format
    - type: str
    - required: true

## snippet

Since the Rscript `clustering_index_args.R` does not explicitly
state an output file, pype can't retrieve which folder/file
should bind in read/write mode when running containerized solutions.
Instead the script takes the input file name and output results files
with the same `dirname` or prefix.
So to workaround this issue, we counterintuitively add the `input`
argument within the _output_ section; this is potentially dangerous,
eg. it may delete/rewrite input files which generally are mounted in
read-only mode.

> _input_: profile_chr_sizes_file profile_cent_tel_file
> _output_: input results_clusters results_dist_pvals

```bash
@/bin/sh, cluster_sv, namespace=csvc

set -e

bedpe='%(input)s'
chr_sizes_file='%(profile_chr_sizes_file)s'
cent_tel_file='%(profile_cent_tel_file)s'

Rscript /opt/Rscripts/clustering_index_args.R \
    $bedpe $chr_sizes_file $cent_tel_file %(requirements_ncpu)i
```
