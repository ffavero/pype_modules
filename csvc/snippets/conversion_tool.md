# Convert Sequenza segments in the rgs format 

Uses the cellularity and ploidy estimated by sequenza
to convert segments in normalized absolute copy numbers

## description

Convert sequenza segments in the rgs format

## requirements

```yaml
ncpu: 1
time: '12:00:00'
mem: 4gb
```

## results

```python
/bin/env python3, json
import json
import os

output_arg = '%(output)s'
sample_id = '%(sample)s'

cn = os.path.join(output_arg, sample_id + '_cn.txt')
segments = os.path.join(output_arg, sample_id + '_segmentation.txt')

res = {'cn': cn, 'segments': segments}

print(json.dumps(res))
```

## arguments

1. sample/i

    - help: Sample ID
    - type: str
    - required: true

2. tumor/t

    - help: Tumor scaled coverage file
    - type: str
    - required: true

3. normal/n

    - help: Normal/control scaled coverage file
    - type: str
    - required: true

4. segs/s

    - help: Segmentation file from Sequenza results
    - type: str
    - required: true

5. cellularity/c

    - help: Cellularity value (fraction from 0 to 1)
    - type: float
    - required: true

6. ploidy/p

    - help: Ploidy value (eg 2 for diploid)
    - type: float
    - required: true

7. output/o

    - help: Output folder
    - type: str
    - required: true

## snippet

> _input_: normal tumor segs
> _output_: output results_cn results_segments

```bash
@/bin/sh, conversion_cov, namespace=csvc

set -e

id='%(sample)s'
control='%(normal)s'
tumor='%(tumor)s'
segs='%(segs)s'
ploidy='%(ploidy)s'
acf='%(cellularity)s'
output='%(output)s'

Rscript /opt/Rscripts/rg_cns_inputGenerator.R \
    -i $id -c $control \
    -t $tumor -s $segs \
    -p $ploidy -a $acf \
    -o $output

```
