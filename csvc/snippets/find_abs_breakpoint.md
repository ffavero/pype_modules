# cSVc Find absolute breakpoint

Uses BRASS perl modules to re-calculate absolute breakpoints
from a BAM file, given a bedpe input file

## description

Find absolute breakpoint from SV

## requirements

```yaml
ncpu: 1
time: '12:00:00'
mem: 4gb
```

## results

```bash
@/bin/sh, yaml


printf  'bedpe: %(out)s'
```

## arguments

1. bam/b

    - help: Bam file used to call SVs
    - type: str
    - required: true

2. bedpe/p

    - help: SV calls in bedpe format
    - type: str
    - required: true

3. out/o

    - help: output file
    - type: str
    - required: true

## snippet

> _input_: bam* bedpe profile_genome_fa*
> _output_: results_bedpe

```bash
@/bin/sh, abs_bkp, namespace=csvc

set -e

bam='%(bam)s'
bedpe='%(bedpe)s'
out='%(out)s'

genome_fa='%(profile_genome_fa)s'

get_abs_bkpts_from_clipped_reads.pl \
    --out $out --fasta $genome_fa \
    $bam $bedpe

```
