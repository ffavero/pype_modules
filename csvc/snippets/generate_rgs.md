# Generate the RGS file for the footprint signature 

Generate the RGS file, which is the semi-final
results of the pipeline.
On its own the rgs file can also beused for a granular inspection of copy-number
around the structural variants breakpoints.

## description

Generate the RGS file used to compute the footprint signature.

## requirements

```yaml
ncpu: 1
time: '12:00:00'
mem: 4gb
```

## results

```python
/bin/env python3, json
import json
import os

cn_file = '%(cn)s'

res = {'rgs': cn_file + '.rg_cns'}

print(json.dumps(res))
```

## arguments

1. bedpe/b

    - help: Breakpoints in bedpe format
    - type: str
    - required: true

2. cn

    - help: Binned copy number file
    - type: str
    - required: true

3. segments/s

    - help: Segmentation file
    - type: str
    - required: true

4. tumor_bam/t

    - help: Tumor BAM file
    - type: str
    - required: true

5. cellularity/c

    - help: Cellularity value (fraction from 0 to 1)
    - type: float
    - required: true

6. ploidy/p

    - help: Ploidy value (eg 2 for diploid)
    - type: float
    - required: true

7. sample/i

    - help: Sample ID
    - type: str
    - required: true

8. gender_present

    - help: Gender present
    - type: str
    - default: N

9. gender_chromosome

    - help: Gender Chromosome
    - type: str
    - default: N

10. tmp

    - help: Working directory for temporary files
    - type: str
    - required: true

## snippet

Again, the unappy implementation in the Rscript `get_rg_cns.R`
does not explicitly state an output file, pype can't retrieve
which folder/file should bind in read/write mode when running
containerized solutions. Instead the script takes the `cn` file
name and output a file with the same prefix, appending a different
exension.

To workaround this issue, we counterintuitively add the `cn` argument
within the output section; this is potentially dangerous,
eg. it may delete/rewrite input files which generally are mounted
in read-only mode.

> _input_: tumor_bam* bedpe segments
> profile_cent_tel_file2

> _output_: tmp results_rgs cn

```bash
@/bin/sh, generate_rgs, namespace=csvc

set -e

id='%(sample)s'
tumor_bam='%(tumor_bam)s'
segs='%(segments)s'
cn='%(cn)s'
bedpe='%(bedpe)s'
ploidy='%(ploidy)s'
acf='%(cellularity)s'
tmp='%(tmp)s'
gender_chromosome='%(gender_chromosome)s'
gender_present='%(gender_present)s'

cent_tel_file='%(profile_cent_tel_file2)s'

Rscript /opt/Rscripts/get_rg_cns.R \
    $bedpe $cn $segs \
    $tumor_bam $acf $cent_tel_file \
    $gender_chromosome \
    $gender_present $tmp

```
