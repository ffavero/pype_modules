# Run somaticSeq

Merge variant calls with somaticseq

## description

Merge variant calls with somaticseq

## requirements

```yaml
ncpu: 24
time: '24:00:00'
mem: 128gb
```

## results

```python
@/bin/env python3, json

import os
import json

outdir = '%(outdir)s'

res = {
    'snvs_vcf': os.path.join(outdir, 'Consensus.sSNV.vcf'),
    'indels_vcf': os.path.join(outdir, 'Consensus.sINDEL.vcf'),
    'snvs_tsv': os.path.join(outdir, 'Ensemble.sSNV.tsv'),
    'indels_tsv': os.path.join(outdir, 'Ensemble.sINDEL.tsv'),
    }

print(json.dumps(res))
```

## name

```bash
@/bin/sh

outdir=`basename %(outdir)s`

echo "somaticseq_$outdir"
```

## arguments

1. normal/n
    - help: Normal BAM file
    - type: str
    - required: true
2. tumor/t
    - help: Tumor BAM file
    - type: str
    - required: true
3. outdir/o
    - help: Output directory
    - type: str
    - required: true
4. mutect/m
    - help: Mutect2 SVNs VCF file
    - type: str
    - required: true
5. strelka_snv/ss
    - help: Strelka Somatic SVNs VCF file
    - type: str
    - required: true
6. strelka_indels/si
    - help: Strelka Somatic INDELs VCF file
    - type: str
    - required: true  

## snippet

> _input_: normal tumor profile_genome_fa*
>          profile_cosmic profile_dbSNP


> _output_: outdir

```bash
@/bin/sh, somaticseq, namespace=somaticseq

outdir=%(outdir)s
normal=%(normal)s
tumor=%(tumor)s
genome=%(profile_genome_fa)s
cosmic=%(profile_cosmic)s
dbsnp=%(profile_dbSNP)s
mutect=%(mutect)s
strelka_snv=%(strelka_snv)s
strelka_indels=%(strelka_indels)s

# todo: decide exclude/include regions
# bed files
# in_regions=
# ex_regions=
#    --inclusion-region  $in_regions \
#    --exclusion-region  $ex_regions \
somaticseq_parallel.py \
    --output-directory  $outdir \
    --genome-reference  $genome \
    --cosmic-vcf        $cosmic \
    --dbsnp-vcf         $dbsnp \
    --algorithm         xgboost \
    --threads           24 \
    paired \
    --tumor-bam-file    $tumor \
    --normal-bam-file   $normal \
    --mutect2-vcf       $mutect \
    --strelka-snv       $strelka_snv \
    --strelka-indel     $strelka_indels

```
