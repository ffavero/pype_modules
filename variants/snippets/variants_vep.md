# Run VEP

Annotate variants with VEP

## description

Annotate variants with the Ensembl Variant Effect Predictor (VEP)

## requirements

```yaml
ncpu: 10
time: '24:00:00'
mem: 128gb
```

## results

```python
@/bin/env python3, json

import os
import json

output = '%(output)s'

res = {
    'output': output
    }

print(json.dumps(res))
```

## name

```bash
@/bin/sh

output=`basename %(output)s`

echo "VEP_$output"
```

## arguments

1. input/i
    - help: Input VCF
    - type: str
    - required: true
2. output/o
    - help: Output VFC
    - type: str
    - required: true

## snippet

> _input_: input profile_genome_fa*
>          profile_vep 


> _output_: output

```bash
@/bin/sh, vep, namespace=vep

ivcf=%(input)s
ovcf=%(output)s
fasta=%(profile_genome_fa)s
annot=%(profile_vep)s

thr=10 #number of cores to use

vep --input_file $ivcf \
    --format vcf \
    --output_file $ovcf \
    --assembly GRCh38 \
    --dir $annot \
    --dir_cache $annot \
    --cache_version 104 \
    --fasta $fasta \
    --everything --fork ${thr} --vcf --cache --offline --dont_skip --pick --stats_text --force_overwrite

```
