# Varlap QC

Varlap is primarily a quality control tool for genetic variants

## description

QC variants with varlap

## requirements

```yaml
ncpu: 1
time: '24:00:00'
mem: 8gb
```

## results

```python
@/bin/env python3, json

import os
import json


res = {
    'variants_csv': '%(output)s'
    }

print(json.dumps(res))
```

## name

```bash
@/bin/sh

sample=%(sample)s

echo varlap_$sample
```

## arguments

1. normal/n
    - help: Normal BAM file
    - type: str
    - required: true
2. tumor/t
    - help: Tumor BAM file
    - type: str
    - required: true
3. vcf/v
    - help: Input VCF file
    - type: str
    - required: true
4. sample/s
    - help: Sample ID
    - type: str
    - required: true
5. output/o
    - help: Varlap CSV output file
    - type: str
    - required: true
6. varclass
    - help: Indels or SNV
    - type: str
    - choices: SNV INDEL
    - default: SNV

## snippet

> _input_: normal tumor vcf
> _output_: output

```bash
@/bin/sh, varlap, namespace=varlap

output=%(output)s
normal=%(normal)s
tumor=%(tumor)s
vcf=%(vcf)s
varclass=%(varclass)s
sample=%(sample)s

# todo: decide include regions bed files
# in_regions=
#    --regions $in_regions \

varlap \
   --sample $sample \
   --varclass $varclass \
   --format VCF \
   --labels tumor normal \
   -- $tumor $normal < $vcf > $output
```
