info:
  description: Strelka2 and MuTect2 SNV and indels calls and QC
  date:        27/11/2020
  api: 2.0.0
  arguments:
    orientation_model:
      File name for the MuTect2 orientation bias model table
    contamination_table:
      File name for the GATK cross-sample contamination table 
    mutect_vcf_filtered:
      Output filtered VCF file
    pileup_summary_tumor:
      File name for the tumor pileup summary table
    pileup_summary_normal:
      File name for the normal pileup summary table
    mutect_vcf_output:
      MuTect2 unfiltered VCF output file
    mutect_stats_output:
      Merged MuTect2 stats output file
    intervals:
      Scattered regions used to run MuTect2
    bam_normal:
      Input normal BAM files
    bam_tumor:
      Input tumor BAM files
    tmp_dir:
      Temporary directory
    sample_name_normal:
      Sample name of the normal/non-tumor genotype
    sample_name_tumor:
      Sample name of the tumor genotype
    strelka_outdir:
      Strelka2 output folder (preferably name it with the sample name)
    consensus_outdir:
      VCF consensus output folder (preferably name it with the sample name)
    exome:
      Flag to use if the sample is from Exome sequencing
  defaults:
    tmp_dir: /scratch
  batches:
    intervals:
      snippet: gatk_mutect2
      required:
        - --intervals
items:
  - name: variants_varlap
    type: pipeline
    arguments:
      - prefix: --bam_normal
        pipeline_arg: '%(bam_normal)s'
      - prefix: --bam_tumor
        pipeline_arg: '%(bam_tumor)s'
      - prefix: --varlap_snvs_csv
        pipeline_arg: '%(varlap_snvs_csv)s'
      - prefix: --varlap_indels_csv
        pipeline_arg: '%(varlap_indels_csv)s'
      - prefix: --sample_name
        pipeline_arg: '%(sample_name_tumor)s'
      - prefix: --vcf_snvs_input
        pipeline_arg:
          snippet_name: variants_somaticseq
          result_key: snvs_vcf
          result_arguments:
            - prefix : -o
              pipeline_arg: '%(consensus_outdir)s'
        type: composite_arg
      - prefix: --vcf_indels_input
        pipeline_arg:
          snippet_name: variants_somaticseq
          result_key: indels_vcf
          result_arguments:
            - prefix : -o
              pipeline_arg: '%(consensus_outdir)s'
        type: composite_arg         
    dependencies:
      items:
        - name: variants_somaticseq
          type: snippet
          arguments:
            - prefix: --normal
              pipeline_arg: '%(bam_normal)s'
            - prefix: --tumor
              pipeline_arg: '%(bam_tumor)s'
            - prefix: --outdir
              pipeline_arg: '%(consensus_outdir)s'
            - prefix: --mutect
              pipeline_arg: '%(mutect_vcf_filtered)s'
            - prefix: --strelka_snv
              pipeline_arg:
                snippet_name: strelka_somatic
                result_key: snvs
                result_arguments:
                  - prefix : -o
                    pipeline_arg: '%(strelka_outdir)s'
              type: composite_arg        
            - prefix: --strelka_indels
              pipeline_arg:
                snippet_name: strelka_somatic
                result_key: indels
                result_arguments:
                  - prefix : -o
                    pipeline_arg: '%(strelka_outdir)s'
              type: composite_arg
          dependencies:
            items:
              - name: gatk4_mutect2
                type: pipeline
                arguments:
                  - prefix: --orientation_model
                    pipeline_arg: '%(orientation_model)s'
                  - prefix: --contamination_table
                    pipeline_arg: '%(contamination_table)s'
                  - prefix: --stats_output
                    pipeline_arg: '%(mutect_stats_output)s'
                  - prefix: --vcf_filtered
                    pipeline_arg: '%(mutect_vcf_filtered)s'
                  - prefix: --vcf_output
                    pipeline_arg: '%(mutect_vcf_output)s'
                  - prefix: --pileup_summary_tumor
                    pipeline_arg: '%(pileup_summary_tumor)s'
                  - prefix: --pileup_summary_normal
                    pipeline_arg: '%(pileup_summary_normal)s'
                  - prefix: --bam_normal
                    pipeline_arg: '%(bam_normal)s'
                  - prefix: --bam_tumor
                    pipeline_arg: '%(bam_tumor)s'
                  - prefix: --tmp_dir
                    pipeline_arg: '%(tmp_dir)s'
                  - prefix: --intervals
                    pipeline_arg: '%(intervals)s'
                  - prefix: --sample_name_normal
                    pipeline_arg: '%(sample_name_normal)s'
              - name: strelka_variants
                type: pipeline
                arguments:
                  - prefix: --normal
                    pipeline_arg: '%(bam_normal)s'
                  - prefix: --tumor
                    pipeline_arg: '%(bam_tumor)s'
                  - prefix: --outdir
                    pipeline_arg: '%(strelka_outdir)s'
                  - prefix: --exome
                    pipeline_arg: '%(exome)s'
                    action: store_true
