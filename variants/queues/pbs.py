##
# cSpell:word pype, qsub, largs, ncpu, naccesspolicy, cpus, Popen

import os
import shlex
import datetime
import subprocess
from time import sleep
from distutils.spawn import find_executable


def submit(command, snippet_name, requirements, dependencies, log, profile):
    pype_exec = find_executable('pype')
    command = '%s --profile %s snippets --log %s %s' % (
        pype_exec, profile, log.__path__, command)
    log.log.info('Command: %s' % command)
    log.log.info('Requirements: %s' % requirements)
    log.log.info('Dependencies: %s' % dependencies)

    stdout = os.path.join(log.__path__, 'stdout')
    stderr = os.path.join(log.__path__, 'stderr')
    stdout_pbs = os.path.join(log.__path__, 'stdout.pbs')
    stderr_pbs = os.path.join(log.__path__, 'stderr.pbs')

    now = datetime.datetime.now()
    now_plus_10 = now + datetime.timedelta(minutes=10)
    startime_str = now_plus_10.strftime("%H%M.%S")

    log.log.info('Execution qsub into working directory %s' % os.getcwd())
    log.log.info('Redirect stdin/stderr to folder %s' % log.__path__)
    command = '''#!/bin/bash
    exec 1>%s
    exec 2>%s
    exec %s ''' % (stdout, stderr, command)
    log.log.info('Retrive custom group environment variable')
    largs = []
    if len(dependencies) > 0:
        dependencies = [
            'afterok:%s' % dep for dep in dependencies]
        depend = ['-W', 'depend=%s' % ','.join(dependencies)]
        largs += depend
    if 'time' in requirements.keys():
        time = ['-l', 'walltime=%s' % requirements['time']]
        largs += time
    if 'mem' in requirements.keys():
        mem = ['-l', 'mem=%s' % requirements['mem']]
        largs += mem
    if 'type' in requirements.keys():
        if requirements['type'] == 'exclusive':
            exclusive = ['-l', 'naccesspolicy=singlejob']
            largs += exclusive
    if 'ncpu' in requirements.keys():
        try:
            nodes = int(requirements['nodes'])
        except KeyError:
            nodes = 1
        cpus = ['-l', 'nodes=%i:ppn=%i' % (nodes, int(requirements['ncpu']))]
        largs += cpus
    qsub_group = os.environ.get('PYPE_QUEUE_GROUP')
    if qsub_group:
        log.log.info('Custom qsub group set to %s' % qsub_group)
        largs += ['-W', 'group_list=%s' % qsub_group, '-A', qsub_group]
    else:
        log.log.info('Custom qsub group not set')
    echo = "echo '%s'" % command
    qsub = [
        'qsub', '-V', '-o', stdout_pbs, '-e', stderr_pbs, '-d', os.getcwd(),
        '-a', startime_str, '-N', snippet_name] + largs
    echo = shlex.split(echo)
    qsub = shlex.split(' '.join(qsub))
    log.log.info('Process command line with subprocess.Popen: %s | %s' %
                 (' '.join(echo), ' '.join(qsub)))
    echo_proc = subprocess.Popen(echo, stdout=subprocess.PIPE)
    qsub_proc = subprocess.Popen(
        qsub, stdin=echo_proc.stdout, stdout=subprocess.PIPE)
    out = qsub_proc.communicate()[0]
    out = out.strip().decode('UTF-8')
    log.log.info('Results job id: %s' % out)
    sleep(1)
    return(out)
