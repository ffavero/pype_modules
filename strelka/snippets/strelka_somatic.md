# Configure and run Strelka Somatic calls

Run and execute strelka configuration scripts

## description

Strelka Somatic calls

## requirements

```yaml
ncpu: 32
time: '24:00:00'
mem: 128gb
type: exclusive
```

## results

```python
@/bin/env python3, json

import os
import json

outdir = '%(outdir)s'
vardir = os.path.join(
    outdir, 'somatic', 'results', 'variants')

res = {
    'snvs': os.path.join(vardir, 'somatic.snvs.vcf.gz'),
    'indels': os.path.join(vardir, 'somatic.indels.vcf.gz')}

print(json.dumps(res))
```


## name

This will set a	friendly name in the queue job
using the output folder name.                          


```bash
@/bin/sh

outdir=`basename %(outdir)s`

echo "somatic_$outdir"
```


## arguments

1. normal/n
    - help: Normal BAM file
    - type: str
    - required: true
2. tumor/t
    - help: Tumor BAM file
    - type: str
    - required: true
3. outdir/o
    - help: Output directory
    - type: str
    - required: true
4. exome
    - help: Flag setting the callable regions 
            and the running mode for exome
    - action: store_true
  

## snippet


> _input_: normal tumor profile_genome_fa
>          profile_wgs_regions profile_wxs_regions


> _output_: outdir

```bash
@/bin/sh, strelka_config_run, namespace=strelka

outdir=%(outdir)s/somatic
normal=%(normal)s
tumor=%(tumor)s
genome=%(profile_genome_fa)s

if [[ $exome == "True" ]]; then
    regions="--exome --callRegions %(profile_wxs_regions)s"
else
    regions="--callRegions %(profile_wgs_regions)s.gz"
fi

configureStrelkaSomaticWorkflow.py \
    $regions \
    --normalBam $normal \
    --tumorBam $tumor \
    --referenceFasta $genome \
    --runDir $outdir

$outdir/runWorkflow.py -m local -j 32

```
