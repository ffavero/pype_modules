# Configure and run Strelka Germline calls

Run and execute strelka configuration scripts

## description

Strelka Germline calls

## requirements

```yaml
ncpu: 32
time: '24:00:00'
mem: 128gb
type: exclusive
```

## results

```python
@/bin/env python3, json

import os
import json

outdir = '%(outdir)s'
bams = '%(input)s'
vardir = os.path.join(
    outdir, 'germline', 'results', 'variants')

index_base = 1
gvcfs = list()
for bam in bams.split():
    gvcf_i = 'genome.S' + str(index_base) + '.vcf.gz'
    gvcfs.append(os.path.join(vardir, gvcf_i))
    index_base += 1

res = {
    'variants': os.path.join(vardir, 'variants.vcf.gz'),
    'gvcf': gvcfs}

print(json.dumps(res))
```

## name

This will set a friendly name in the queue job
using the output folder name.


```bash
@/bin/sh 

outdir=`basename %(outdir)s`

echo "germline_$outdir"
```

## arguments

1. input/i
    - help: Input BAM file(s)
    - type: str
    - required: true
    - nargs: *
2. outdir/o
    - help: Output direcrory
    - type: str
    - required: true
3. exome
    - help: Flag setting the callable regions 
            and the running mode for exome
    - action: store_true
  

## snippet


> _input_: input profile_genome_fa
>          profile_wgs_regions profile_wxs_regions


> _output_: outdir

```bash
@/bin/sh, strelka_config_run, namespace=strelka

outdir=%(outdir)s/germline
bams="%(input)s"
genome=%(profile_genome_fa)s

if [[ $exome == "True" ]]; then
    regions="--exome --callRegions %(profile_wxs_regions)s"
else
    regions="--callRegions %(profile_wgs_regions)s.gz"
fi

argsbam=''
for bam in $bams; do
    argsbam="$argsbam --bam $bam"
done

configureStrelkaGermlineWorkflow.py \
    $argsbam \
    --referenceFasta $genome \
    --runDir $outdir

$outdir/runWorkflow.py -m local -j 32

```
