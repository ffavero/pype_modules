# Configure and run Manta Somatic calls

Run and execute Manta configuration scripts

## description

Manta somatic SV calls -still a copy of strelka somatic for now-

## requirements

```yaml
ncpu: 48
time: '24:00:00'
mem: 64gb
```

## results

```python
@/bin/env python3, json

import os
import json

outdir = '%(outdir)s'
vardir = os.path.join(
    outdir, 'results', 'variants')

res = {
    'snvs': os.path.join(vardir, 'somatic.snvs.vcf.gz'),
    'indels': os.path.join(vardir, 'somatic.indels.vcf.gz')}

print(json.dumps(res))
```

## arguments

1. normal/n
    - help: Normal BAM file
    - type: str
    - required: true
2. tumor/t
    - help: Tumor BAM file
    - type: str
    - requred: true
3. outdir/o
    - help: Output direcrory
    - type: str
    - required: true
4. exome
    - help: Flag setting the callable regions 
            and the running mode for exome
    - action: store_true
  

## snippet


> _input_: normal tumor profile_genome_fa
>          profile_wgs_regions profile_wxs_regions


> _output_: outdir

```bash
@/bin/sh, strelka_config, namespace=strelka

outdir=%(outdir)s
normal=%(normal)s
tumor=%(tumor)s
genome=%(profile_genome_fa)s

if [[ $exome == "True" ]]; then
    regions="--exome --callRegions %(profile_wxs_regions)s"
else
    regions="--callRegions %(profile_wgs_regions)s.gz"
fi

configureStrelkaSomaticWorkflow.py \
    $regions \
    --normalBam $normal \
    --tumorBam $tumor \
    --referenceFasta $genome \
    --runDir $outdir

$outdir/runWorkflow.py -m local -j 20

```
