# BIO-pype modules

Various module sets implementing bioinfomratics pipelines


# install

Install a module set with the `pype repos` command


```
pype repos -r https://bitbucket.org/ffavero/pype_modules/raw/master/repos.yaml install <subproject_name>
```


Then you need to adjust the `yaml` profile of your choice 
to fit the HPC/cloud configuration
