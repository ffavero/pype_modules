# FastQC snippet

A simple implementation of the fastQC command line tool


## description

FastQ QC with fastQC

## requirements

```yaml
ncpu: 4
time: '24:00:00'
mem: 64gb
```

## results

```python
@/bin/env python3, json

import os
import json

fastqs='%(fastq)s'
outdir='%(outdir)s'

zips=list()
htmls=list()
for fastq in fastqs.split():
    fastq = fastq.strip()
    file_prefix = os.path.basename(fastq)
    file_prefix = file_prefix.replace('.gz', '')
    file_prefix = file_prefix.replace('.fastq', '')
    if outdir == 'None':
        outdir = os.path.dirname(fastq)
    htmls.append(os.path.join(
        outdir, file_prefix + '_fastqc.html'))
    zips.append(os.path.join(
        outdir, file_prefix + '_fastqc.zip'))

res = {'report': htmls, 'data': zips}
print(json.dumps(res))
```

## arguments

1. fastq/f
    - help: Fastq files
    - type: str
    - required: true
    - nargs: *
2. outdir/o
    - help: QC output folder
    - type: str
3. nanopore
    - help: Is the fastQ from a nanopore sequencing
    - action: store_true
  

## snippet


> _input_: fastq


> _output_: results_report results_data

```bash
@/bin/sh, fastqc_report, namespace=fastqc

outdir=%(outdir)s
nano=%(nanopore)s


extra_cmd=""
if [[ $outdir != "None" ]]; then
    extra_cmd="$extra_cmd --outdir $outdir"
fi
if [[ $nano == "True" ]]; then
    extra_cmd="$extra_cmd --nano"
fi

fastqc --quiet --threads 4 $extra_cmd %(fastq)s

```
