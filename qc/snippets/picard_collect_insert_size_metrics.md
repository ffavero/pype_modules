# Picard Collect Insert Size Metrics

Insert Size Metrics from picard tools

## description

BAM QC with Picard CollectInsertSizeMetrics

## requirements

```yaml
ncpu: 1
time: '12:00:00'
mem: 8gb
```

## results

```python
/bin/env python3, json
import json
import os

out_path = '%(output)s'
sample_id = '%(sample)s'
prefix = os.path.join(out_path, sample_id)


res = {
    'insert_size_metrics': prefix + '_insert_size_metrics.txt',
    'insert_size_histogram': prefix + '_insert_size_histogram.pdf'}

print(json.dumps(res))
```

## name

```python
@/bin/env python3

sample_id = '%(sample)s'
print('picard_collect_insert_size_'+ sample_id)
```

## arguments

1. input/i
    - help: BAM input file
    - type: str
    - required: true
2. output/o
    - help: Output folder
    - type: str
    - required: true
3. sample/s
    - help: Sample name or identifier
    - type: str
    - required: true

## snippet


> _input_: profile_genome_fa input
> _output_: output

```bash
@/bin/sh, picard_collect_insert_size, namespace=picard

set -e

sample_id='%(sample)s'
out_dir='%(output)s'

prefix=$out_dir/$sample_id

gatk --java-options "-Xmx%(requirements_mem)im" \
    CollectInsertSizeMetrics \
    I=%(input)s \
    O=$prefix\_insert_size_metrics.txt \
    H=$prefix\_insert_size_histogram.pdf \
    M=0.5
```