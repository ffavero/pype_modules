# MultiQC

Report QC and sequencing results with multiQC

## description

Report generations with MultiQC

## requirements

```yaml
ncpu: 1
time: '12:00:00'
mem: 8gb
```

## results

```python
/bin/env python3, json
import json
import os

out_path = '%(output)s'
sample_id = '%(sample)s'
prefix = os.path.join(out_path, sample_id)
res = {
    'report': prefix + '_multiqc.html'}

print(json.dumps(res))
```

## name

```python
@/bin/env python3

sample_id = '%(sample)s'
print('multiqc_'+ sample_id)
```

## arguments

1. input/i
    - help: Input folder(s)
    - type: str
    - required: true
    - nargs: *
2. output/o
    - help: Output folder
    - type: str
    - required: true
3. sample/s
    - help: Sample name or identifier
    - type: str
    - required: true

## snippet


> _input_: input
> _output_: output

```bash
@/bin/sh, multiqc, namespace=multiqc

set -e

id='%(sample)s'
out_dir='%(output)s'

prefix_out=$out_dir/$id

multiqc --force %(input)s \
    --filename $prefix_out\_multiqc.html
```