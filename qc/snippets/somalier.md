# Somalier

Rapid relatedness estimation tool

## description

Extract genotype-like information for a single-sample at selected sites

## requirements

```yaml
ncpu: 1
time: '12:00:00'
mem: 8gb
```

## results

```python
/bin/env python3, json
import json
import os

out_path = '%(output)s'
sample_id = '%(sample)s'
prefix = os.path.join(out_path, sample_id)
res = {'somalier': prefix + '.somalier'}

print(json.dumps(res))
```

## name

```python
@/bin/env python3

sample_id = '%(sample)s'
print('somalier_'+ sample_id)
```


## arguments

1. input/i
    - help: BAM input file
    - type: str
    - required: true
2. output/o
    - help: Output folder
    - type: str
    - required: true
3. sample/s
    - help: Sample name or identifier
    - type: str
    - required: true

## snippet


> _input_: profile_genome_fa input
> _output_: output

```bash
@/bin/sh, somalier, namespace=somalier

set -e

id='%(sample)s'
out_dir='%(output)s'

prefix_out=$out_dir

somalier extract --sites %(profile_somalier_sites)s \
    --fasta %(profile_genome_fa)s \
    --out-dir $prefix_out \
	--sample-prefix $id \
    %(input)s

```
