# FastQC

The QC for fastQC files

## description

FastQ QC reports

## requirements

```yaml
ncpu: 1
time: '12:00:00'
mem: 8gb
```

## results

```python
/bin/env python3, json
import json
import os

res = {}
outdir = '%(output)s'
fq1 = '%(f1)s'
fq2 = '%(f2)s'

fq1ResultPrefix = os.path.join(
    outdir, os.path.basename(fq1).replace('.fastq.gz', ''))

res['fq1HtmlReport'] =  fq1ResultPrefix + '_fastqc.html' 
res['fq1ZipReport'] =  fq1ResultPrefix + '_fastqc.zip'

if fq2 is not 'None':
    fq2ResultPrefix = os.path.join(
        outdir, os.path.basename(fq2).replace('.fastq.gz', ''))
    res['fq2HtmlReport'] =  fq2ResultPrefix + '_fastqc.html' 
    res['fq2ZipReport'] =  fq2ResultPrefix + '_fastqc.zip'


print(json.dumps(res))
```

## name

```python
@/bin/env python3

f1 = '%(f1)s'
print('fastqc_'+ f1)
```

## arguments

1. f1/1
    - help: BAM input file
    - type: str
    - required: true
2. f2/2
    - help: Output folder
    - type: str
    - required: false
3. output/o
    - help: Output directory
    - type: str
    - required: true
4. tmp/t
    - help: Temporary folder
    - type: str
    - default: /scratch


## snippet


> _input_: f1 f2
> _output_: output tmp

```bash
@/bin/sh, fastqc, namespace=fastqc

set -e

out_dir=%(output)s
f1=%(f1)s
f2=%(f2)s

if [[ "$f2" == None ]]; then
    fastq=$f1
else
    fastq="$f1 $f2"
fi

fastqc --outdir $out_dir \
    --threads %(requirements_ncpu)i \
    --dir %(tmp)s $fastq
```