# Cov tools and plot for single BAM

Coverage plots for QC inspection

## description

Coverage plot for a single BAM file

## requirements

```yaml
ncpu: 1
time: '12:00:00'
mem: 8gb
```

## results

```python
/bin/env python3, json
import json
import os

out_path = '%(output)s'
sample_id = '%(sample)s'
prefix = os.path.join(out_path, sample_id)
res = {
    'cov_file': prefix + '_10k_cov.gz',
    'cov_plot': prefix + '_10k_cov_plot.pdf'}

print(json.dumps(res))
```

## name

```python
@/bin/env python3

sample_id = '%(sample)s'
print('cov_tool_10k_'+ sample_id)
```

## arguments

1. input/i
    - help: BAM input file
    - type: str
    - required: true
2. output/o
    - help: Output folder
    - type: str
    - required: true
3. sample/s
    - help: Sample name or identifier
    - type: str
    - required: true

## snippet


> _input_: input
> _output_: results_cov_file

```bash
@/bin/sh, cov_tool, namespace=cov_tool

set -e

id='%(sample)s'
out_dir='%(output)s'

cov -s 10000 -o 10000 -f %(results_cov_file)s %(input)s
```

*TODO*: We may need to scale and  GC normalize the coverage file.

> _input_: results_cov_file
> _output_: results_cov_plot

```bash
@/bin/env Rscript, cov_plot, namespace=R

sample_name <- "%(sample)s"
out_dir <- "%(output)s"
cov_file <- "%(results_cov_file)s"
output_file <- file.path(
    out_dir, paste(sample_name, "cov_plot.pdf", sep = "_"))

# nip and tuck from Joachim cov plot script

cov1_full <- read.table(cov_file)
pdf("%(results_cov_plot)s", width = 18, height = 5);
chr = unique(cov1_full[, 1])

for(myChr in chr){
    if (grepl("GL|NC|hs|alt|decoy|random|KI|chrUn|EBV|HLA", myChr, perl=T)){
        next
    } else {
        cat ("chromosome:",myChr, "\n")
        cov1 <-  cov1_full[cov1_full[, 1] == myChr,]
        logCov <- log2(cov1[, 5])
        windowsMerged <- 1
	winSizeInKb <- (cov1[1, 3] - cov1[1, 2]) / 1000 * windowsMerged

        xnames <- cov1[, 2]
        ymax <- max(logCov[is.finite(logCov)])

        plot(
            xnames, logCov, xlim = c(min(xnames), max(xnames)), ylim = c(0, ymax),
            main = paste(sample_name, myChr), xlab = "Chromosome position",
            ylab = paste("Log2 #read per ", winSizeInKb, "kb", sep = ""), col='black',
            pch = 20, cex.main = 1.5, cex.axis = 1.5, cex.lab = 1.5, cex = 0.8, las = 1)
        abline(h = median(logCov[is.finite(logCov)]), col = "gray77", lty = 1)
        abline(h = seq(0, (as.integer(ymax) + 1), 2), col = "gray60", lty = "dotted")
        abline(v = seq(min(xnames), max(xnames), 5000000), col = "gray60", lty = "dotted")
    }
}
dev.off()
```
