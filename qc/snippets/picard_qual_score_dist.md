# Picard Quality Score Distribution

Quality Score Distribution from picard tools

## description

BAM QC with Picard QualityScoreDistribution

## requirements

```yaml
ncpu: 1
time: '12:00:00'
mem: 8gb
```

## results

```python
/bin/env python3, json
import json
import os

out_path = '%(output)s'
sample_id = '%(sample)s'
prefix = os.path.join(out_path, sample_id)

res = {
    'qual_score_dist': prefix + '_qual_score_dist.txt',
    'qual_score_dist_plot': prefix + '_qual_score_dist.pdf'}

print(json.dumps(res))
```

## name

```python
@/bin/env python3

sample_id = '%(sample)s'
print('picard_quality_dist_'+ sample_id)
```

## arguments

1. input/i
    - help: BAM input file
    - type: str
    - required: true
2. output/o
    - help: Output folder
    - type: str
    - required: true
3. sample/s
    - help: Sample name or identifier
    - type: str
    - required: true

## snippet


> _input_: profile_genome_fa input
> _output_: output

```bash
@/bin/sh, picard_quality_dist, namespace=picard

set -e

sample_id='%(sample)s'
out_dir='%(output)s'

prefix=$out_dir/$sample_id

gatk --java-options "-Xmx%(requirements_mem)im" \
    QualityScoreDistribution \
    I=%(input)s \
    O=$prefix\_qual_score_dist.txt \
    CHART=$prefix\_qual_score_dist.pdf
```