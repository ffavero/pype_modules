# Picard CollectGcBiasMetrics

Gc Bias Metrics from picard tools

## description

BAM QC with Picard CollectGcBiasMetrics

## requirements

```yaml
ncpu: 1
time: '12:00:00'
mem: 8gb
```

## results

```python
/bin/env python3, json
import json
import os

out_path = '%(output)s'
sample_id = '%(sample)s'
prefix = os.path.join(out_path, sample_id)

res = {
    'summary_metrics': prefix + '_summary_metrics.txt',
    'gc_bias_metrics': prefix + '_gc_bias_metrics.txt',
    'gc_bias_metrics_plot': prefix + '_gc_bias_metrics.pdf'}

print(json.dumps(res))
```

## arguments

1. input/i
    - help: BAM input file
    - type: str
    - required: true
2. output/o
    - help: Output folder
    - type: str
    - required: true
3. sample/s
    - help: Sample name or identifier
    - type: str
    - required: true

## name

```python
@/bin/env python3

sample_id = '%(sample)s'
print('picard_collect_gc_bias_'+ sample_id)
```

## snippet


> _input_: profile_genome_fa input
> _output_: output

```bash
@/bin/sh, picard_collect_gc_bias, namespace=picard

set -e

sample_id='%(sample)s'
out_dir='%(output)s'

prefix=$out_dir/$sample_id

gatk --java-options "-Xmx%(requirements_mem)im" \
    CollectGcBiasMetrics \
    I=%(input)s \
    O=$prefix\_gc_bias_metrics.txt \
    CHART=$prefix\_gc_bias_metrics.pdf \
    S=$prefix\_summary_metrics.txt \
    R=%(profile_genome_fa)s
```