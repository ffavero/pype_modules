# Mosdepth BAM QC

Coverage and other BAM QC metrics with mosdepth

## description

BAM QC with Mosdepth

## requirements

```yaml
ncpu: 1
time: '12:00:00'
mem: 8gb
```

## results

```python
/bin/env python3, json
import json
import os

out_path = '%(output)s'
sample_id = '%(sample)s'
prefix = os.path.join(out_path, sample_id)
res = {
    'dist': prefix + '.mosdepth.global.dist.txt',
    'summary': prefix + '.mosdepth.summary.txt',
    'bed': prefix + '.per-base.bed.gz'}

print(json.dumps(res))
```

## name

```python
@/bin/env python3

sample_id = '%(sample)s'
print('mosdepth_'+ sample_id)
```

## arguments

1. input/i
    - help: BAM input file
    - type: str
    - required: true
2. output/o
    - help: Output folder
    - type: str
    - required: true
3. sample/s
    - help: Sample name or identifier
    - type: str
    - required: true

## snippet


> _input_: profile_genome_fa input
> _output_: output

```bash
@/bin/sh, mosdepth, namespace=mosdepth

set -e

id='%(sample)s'
out_dir='%(output)s'

prefix_out=$out_dir/$id

mosdepth -t %(requirements_ncpu)i \
    -f %(profile_genome_fa)s $prefix_out \
    %(input)s

```