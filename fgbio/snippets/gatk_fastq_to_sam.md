
 [//]: # ( cSpell:word funcotator, gatk, ncpu, germline, exome )

# Merge fastq into unmapped BAM

Picard Fastq To Sam

## description

Picard FastqToSam

## requirements

```yaml
ncpu: 1
time: '24:00:00'
mem: 8gb
```

## results

```python
@/usr/bin/env python3, json

import os
import json

output = '%(output)s'

res = {'bam': output}

print(json.dumps(res))
```

## name

```bash
@/bin/sh

output=`basename %(output)s`

echo "fastq_to_bam_$output"
```

## arguments

1. sample_name/s
    - help: Sample name/Aliquot IDs
    - type: str
    - required: true
2. fastq1/1
    - help: First Mate of the fastq pair
    - type: str
    - required: true
3. fastq2/2
    - help: Second mate of the fastq pair
    - type: str
    - required: true
4. output/o
    - help: Unaligned BAM output
    - type: str
    - required: true

## snippet

> _input_: fastq1 fastq2

> _output_: output

```bash
@/bin/sh, FastqToSam, namespace=gatk4

output=%(output)s
f1=%(fastq1)s
f2=%(fastq2)s
sample_name=%(sample_name)s

gatk --java-options "-Xmx%(requirements_mem)im" \
   FastqToSam \
   FASTQ=$f1 \
   FASTQ2=$f2 \
   O=$output \
   SM=$sample_name
```
