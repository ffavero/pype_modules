
 [//]: # ( cSpell:word funcotator, gatk, ncpu, germline, exome )

# Mark Duplicates with UMI

Picard MarkDuplicates using UMI barcodex in the RX tag

## description

Picard MarkDuplicates  using UMI barcodes

## requirements

```yaml
ncpu: 1
time: '24:00:00'
mem: 8gb
```

## results

```python
@/usr/bin/env python3, json

import os
import json

output = '%(output)s'

res = {'bam': output}

print(json.dumps(res))
```

## name

```bash
@/bin/sh

output=`basename %(output)s`

echo "mark_duplicates_umi_$output"
```

## arguments

1. input/i
    - help: Input BAM file(s)
    - type: str
    - required: true
    - nargs: *
2. output/o
    - help: Mark duplicates BAM output
    - type: str
    - required: true
3. metrics/m
    - help: Mark duplicates metrics output
    - type: str
    - required: true

## snippet

> _input_: input

> _output_: output metrics

```bash
@/bin/sh, MarkDuplicates, namespace=gatk4

output=%(output)s
input=%(input)s
metrics=%(metrics)s

gatk --java-options "-Xmx%(requirements_mem)im" \
   MarkDuplicates \
   -I $input \
   -O $output \
   -M $metrics \
   --BARCODE_TAG RX \
   --CREATE_INDEX true \
   --CREATE_MD5_FILE true \
   --DUPLEX_UMI true \
   --MOLECULAR_IDENTIFIER_TAG RX
```
