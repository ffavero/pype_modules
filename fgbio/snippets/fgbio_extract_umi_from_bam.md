
 [//]: # ( cSpell:word funcotator, gatk, ncpu, germline, exome )

# Excteact UMI from BAM

fgbio ExtractUmisFromBam

## description

fgbio ExtractUmisFromBam

## requirements

```yaml
ncpu: 1
time: '24:00:00'
mem: 8gb
```

## results

```python
@/usr/bin/env python3, json

import os
import json

output = '%(output)s'

res = {'bam': output}

print(json.dumps(res))
```

## name

```bash
@/bin/sh

output=`basename %(output)s`

echo "extract_umi_$output"
```

## arguments

1. input/i
    - help: Input unaligned Bam
    - type: str
    - required: true
2. output/o
    - help: Output uniligned BAM (UMI extracted)
    - type: str
    - required: true
3. read_structure1/s1
    - help: First mate read structure using fgbio notation
    - type: str
    - default: 8M143T
4. read_structure2/s2
    - help: Second mate read structure using fgbio notation
    - type: str
    - default: 8M143T
5. umi_tag1/u1
    - help: molecular index tag for the first mate
    - type: str
    - default: ZA
6. umi_tag2/u2
    - help: molecular index tag for the second mate
    - type: str
    - default: ZB

## snippet

> _input_: input

> _output_: output

```bash
@/bin/sh, ExtractUmisFromBam, namespace=fgbio

output=%(output)s
input=%(input)s
u1=%(umi_tag1)s
u2=%(umi_tag2)s

s1=%(read_structure1)s
s2=%(read_structure2)s

java  -Xmx4g -XX:+AggressiveOpts -XX:+AggressiveHeap -jar $FGBIO/fgbio.jar ExtractUmisFromBam \
   --input=$input --output=$output \
   --read-structure=$s1 $s2 --molecular-index-tags=$u1 $u2 \
   --single-tag=RX
```
