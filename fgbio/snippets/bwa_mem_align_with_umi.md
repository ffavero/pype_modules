
 [//]: # ( cSpell:word funcotator, gatk, ncpu, germline, exome )

# Merge fastq into unmapped BAM

Picard Fastq To Sam

## description

Picard FastqToSam

## requirements

```yaml
ncpu: 8
time: '24:00:00'
mem: 32gb
```

## results

```python
@/usr/bin/env python3, json

import os
import json

output = '%(output)s'

res = {'bam': output}

print(json.dumps(res))
```

## name

```bash
@/bin/sh

output=`basename %(output)s`

echo "fastq_to_bam_$output"
```

## arguments

1. input/i
    - help: Unaligned input BAM
    - type: str
    - required: true
2. output/o
    - help: Aligned output BAM
    - type: str
    - required: true

## snippet

> _input_: input

```bash
@/bin/sh, SamToFastq, stdout=bwa_mem, namespace=gatk4

input=%(input)s
all_mem=%(requirements_mem)i
step_mem="$((all_mem/3))m"
gatk --java-options "-Xmx$step_mem" \
   SamToFastq \
   -I $input \
   -F /dev/stdout \
   --INTERLEAVE true
```

> _input_: profile_genome_fa

```bash
@/bin/sh, bwa_mem, stdout=MergeBamAlignment, namespace=bwa

npcus=$((%(requirements_ncpu)i-3))

genome=%(profile_genome_fa)s
bwa mem -p -t $npcus $genome /dev/stdin

```

> _input_: profile_genome_fa input
> _output_: output

```bash
@/bin/sh, MergeBamAlignment, namespace=gatk4

output=%(output)s
input_unmapped=%(input)s
genome=%(profile_genome_fa)s
all_mem=%(requirements_mem)i
step_mem="$((all_mem/3))m"
gatk --java-options "-Xmx$step_mem" \
    MergeBamAlignment \
    -UNMAPPED $input_unmapped -ALIGNED /dev/stdin -O $output -R $genome \
    -SO coordinate --ALIGNER_PROPER_PAIR_FLAGS true -MAX_GAPS -1 \
    -ORIENTATIONS FR --VALIDATION_STRINGENCY SILENT --CREATE_INDEX true \
    --CREATE_MD5_FILE true

```

